import React, { Component } from 'react';
import { View,Dimensions ,Text,StyleSheet,TouchableWithoutFeedback,ImageBackground,Image,FlatList,TouchableOpacity} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { ScrollView } from 'react-native-gesture-handler';
import ElevatedView from 'react-native-elevated-view'
import { withNavigation,createDrawerNavigator, DrawerItems,createAppContainer } from 'react-navigation';
import Header from './Header';
import LinearGradient from 'react-native-linear-gradient';
import { Form, Item, Picker } from 'native-base';
import { ActivityIndicator } from 'react-native-paper';
import FAB from 'react-native-fab'
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';
import Sidebar from './Sidebar'


var {height, width} = Dimensions.get('window');
 class Alert extends Component{ 
    constructor(){
      super(); 
      this.state={
          layout:{
              height:height,
              width:width, 
            },
            baseURL:'http://api.neutralairpartner.com',
            image:'',
            casApi:'/v1/cas',
            selected2: 'key0', 
            alerts:[],  
            isloading:true,
            name:[],
            currency:[], 
           countryname:[],
           refresh:[],
           statusImage:'./Nap_Ios/tick.png'
              
      } 
    }

    componentWillMount(){
        // if(this.state.selected2=='key0'){
        //     this.onFetchAlertInfo(global.token)
        // } else{
        //     alert('2')
        // } 
    }

    onRefresh=async()=>{
      this.setState({
        refresh: true
      });
      this.componentWillMount()
    }
    //for alert info
    async onFetchAlertInfo(token){ 
        try {
          let response = await fetch(
           this.state.baseURL+this.state.casApi,
           {
             'method': 'GET',
             headers: {
               'Accept': 'application/json',
              'Content-Type': 'application/json',
              'Authorization':'Bearer'+' '+token
              },
          }
         );
          if (response.status >= 200 && response.status < 300) {
            response.json().then(data => {
              data.map((item,key)=>{
                this.state.alerts.push(item)

                
                 
                 
                let currencyUrl= item.currency  
                let url=  item.company 
                this.onfetchCompany(global.token, url)
                this.onfetchCurrency(global.token, currencyUrl)
                
                })
               
                
              }); 
           } else{
             alert('JWT token not found')
           }
        } catch (errors) {
           alert(errors);
          } 
        }

        //for company name
        async onfetchCompany(token,url){
          try {
            let response = await fetch(
             this.state.baseURL+url,  
             {
               'method': 'GET', 
               headers: {
                 'Accept': 'application/json', 
                'Content-Type': 'application/json',
                'Authorization':'Bearer'+' '+token 
                },
            }
           ); 
            if (response.status >= 200 && response.status < 300) {
              response.json().then(data => {
               this.state.name.push(data.companyName.slice(0,19))
                let countryUrl= data.countries
              
                //this.onfetchCountries(global.token, countryUrl)
                }); 
                this.setState({refresh:true})
                this.setState({isloading:false})
             } else{
               alert('JWT token not found')
             }
          } catch (errors) {
             alert(errors);
            }  
        } 

        //for currency
        async onfetchCurrency(token,url){
          try {
            let response = await fetch(
             this.state.baseURL+url,
             { 
               'method': 'GET', 
               headers: {
                 'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization':'Bearer'+' '+token
                },
            }
           );
            if (response.status >= 200 && response.status < 300) {
              response.json().then(data => {
                this.state.currency.push(data.code)
                // console.log(JSON.stringify(data.id))
                
                }); 
                
             } else{
               alert('JWT token not found')
             }
          } catch (errors) {
             alert(errors);
            } 
        }

        //for countries
        async onfetchCountries(token,url){
          try {
            let response = await fetch(
             this.state.baseURL+url,
             { 
               'method': 'GET',
               headers: {
                 'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization':'Bearer'+' '+token
                },
            }
           );
            if (response.status >= 200 && response.status < 300) { 
              response.json().then(data => {
                this.state.countryname.push(data.countryname)
                }); 
                
             } else{
               alert('JWT token not found')
             }
          } catch (errors) {
             alert(errors);
            } 
        }


    onValueChange2(value) {
        this.setState({
          selected2: value
        }); 
      }
  
    render() {
      if (this.state.isloading) {
        return (
          <ActivityIndicator 
              animating={true} 
              color={'#303147'} 
              size={'large'}
              style={{ position:'absolute', left:0, right:0, bottom:0, top:0 }}/>
         );
       } else{
        return(
         
            <View style={{height:this.state.layout.height, width: this.state.layout.width, backgroundColor:'#f9f9f9',alignItems:"center"}}>
               <Header/>
               <ScrollView>
                <Form style={{marginTop:hp('2%'), width:'92%',alignSelf:'center'}}>
                <Item picker rounded style={{backgroundColor:'#0099cc'}}>
                <Picker 
                mode="dropdown"
                // iosIcon={}
                style={{ width: '100%' }}
                textStyle={{color:'white',fontFamily:'Montserrat-Bold', fontSize:13}}
                selectedValue={this.state.selected2}
                onValueChange={this.onValueChange2.bind(this)}>
                <Picker.Item label="Show All Alerts" value="key0" />
                <Picker.Item label="Show Pending Only Alerts" value="key1" />
                <Picker.Item label="Show Resolved Only Alerts" value="key2" />
                <Picker.Item label="Show Declined Only Alerts" value="key3" />
                
                </Picker>
                 </Item>
                </Form> 
 
         
                 
                 
                <View style={{width: this.state.layout.width, height: hp('13%'),backgroundColor:'white',marginTop:hp('1%')}}> 
                      <TouchableOpacity style={{width:wp('40%'),height:hp('7%'),alignSelf:'center',backgroundColor:'transparent',}}>
                        <View  style={{width:'100%',height:'100%',borderRadius:45,alignItems:'center',backgroundColor:'#C1DBE3'}}>
                        <Text style={{fontSize:15,fontFamily:'Montserrat-Light',marginTop:hp('1.5%'),color:'black'}}>Support Center</Text>
                        </View>
                       </TouchableOpacity>
                       <View style={{width: this.state.layout.width/1.4, height:hp('3%'),flexDirection:'row',justifyContent:'space-around',backgroundColor:'white',alignSelf:'center',marginTop:hp('2%')}}>
                            <Text style={{color:'grey',fontFamily:'Gill Sans',fontSize:14,paddingLeft:'7%'}}>Privacy</Text>
                            <Text style={{color:'grey',fontFamily:'Gill Sans',fontSize:14}}>Terms</Text>
                            <Text style={{color:'grey',fontFamily:'Gill Sans',fontSize:14}}>Credit Terms</Text>
                        </View>
                      </View>
                      </ScrollView>
                      <FAB buttonColor="transparent" iconTextColor='#FFFFFF'  visible={true} iconTextComponent={<Image source={require('./Nap_Ios/createnwew.png')}/>} />
             </View>
              

            
          );
      }}
  }
  
  const AppDrawerNavigator= createDrawerNavigator({
  
    //Home: Home,
    // Promo: Promo,
     Alert: Alert,
    // Geolocator: Geolocator,
    
  },{
    contentComponent: props => <Sidebar />,
    drawerWidth: width/4,
    drawerBackgroundColor:'#303147',
    contentOptions:{
      activeTintColor:'red'
    }
  })
const AppContainer= createAppContainer(AppDrawerNavigator)
  
  var styles = StyleSheet.create({
    
    AlertHead:{
      height:'85%',
      width:'90%',
      backgroundColor:'transparent',
      marginTop:'5%'
    },
    AlertCompanyName:{
      height:'10%',
      width:'95%',
      backgroundColor:'transparent',
      alignSelf:'center',
      flexDirection:'row',
      justifyContent:'space-between'
    },
    HeadText:{
      fontFamily:'Montserrat-Regular',
      fontSize:14,
      color:'black'
    },
    HeadSecondaryText:{
      fontFamily:'Montserrat-Regular',
      fontSize:11,
      color:'black'
    },
    AlertDocument:{
      height:'12%',
      width:'95%',
      backgroundColor:'transparent', 
      alignSelf:'center',
      marginTop:'3%'
    },

    AlertDownloadsView:{
      height:'40%',
      width:'95%',
      backgroundColor:'transparent',
      marginTop:'3%',
      alignSelf:'center',
      justifyContent:'space-around'
    },
    DownloadText:{
      fontFamily:'Montserrat-Light',
      fontSize:12,
      color:'black'
    },
    Boldtext:{
      fontFamily:'Montserrat-Bold',
      fontSize:11,
      color:'black',paddingLeft:'3%',paddingTop:'0.3%'
    }
  
    });

    export default withNavigation(AppContainer);





  