import React, { Component } from 'react';
import { View,Dimensions ,Text,StyleSheet,TouchableOpacity,ImageBackground,Keyboard,} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import LinearGradient from 'react-native-linear-gradient';
import { TextField } from 'react-native-material-textfield';
import { createStackNavigator,createAppContainer,withNavigation } from 'react-navigation';
import { FluidNavigator, Transition } from 'react-navigation-fluid-transitions';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Home from './Home';
import Filter from './Filter';
import Profile from './Profile';
import Header from './Header';
import Promo from './Promo';
import Alert from './Alert';
import Geolocator from './Geolocators';
import Post_Geo from './Post_Geo';
import Post_Promo from './Post_Promo';
import ChatScreen from './ChatScreen';
import Sidebar from './Sidebar';
import HeaderHome from './HeaderHome';
 class Login extends Component{
  constructor(){
    super();
    global.token=null,
    
    this.state={
      hidden: true,
      Email:'',
      Password:'',
      isChecked: false,
      result:{},
      color:'white',
      text:'Remember me',
      
    }
  }

  

  handleEmail=(text)=>{
    this.setState({Email:(text)})
  }

  handlePassword=(text)=>{
    this.setState({Password:(text)})
  }


// api call
  async onFetchLoginRecords() {
    const URL='http://api.neutralairpartner.com/login_check'
    const data={
      username: this.state.Email,
      password: this.state.Password
    }
    try {
     let response = await fetch(
      URL,
      {
        'method': 'POST',
        headers: {
          'Accept': 'application/json',
         'Content-Type': 'application/json',
         },
         body: JSON.stringify(data)
     }
    );
     if (response.status >= 200 && response.status < 300) {
        response.json().then(data => {
        global.token=data.token;
        this.props.navigation.navigate('Home');
    }
    );
      }else{
        alert('Please Check Login Details')
      }
   } catch (errors) {
      alert(errors);
     } 
}

login=(Email,Password)=>{
  if(Email==''|| Password=='')
    {
      alert('Fields Empty');
    }
  else{
    this.onFetchLoginRecords();
    
    }
  }
 

check=()=>{
  this.setState({isChecked: true, color:'grey', text:'Long Press To Dissable'})
}

uncheck=()=>{
  this.setState({isChecked: false, color:'white', text:'Remember me'})
}
    render() {
      return(
          <Transition appear='horizontal' >
            
             <LinearGradient colors={['#303147', '#332863']} 
                startPoint={{x: 5.0, y: 0.0}} endPoint={{x: 5.0, y: 0.0}}
                style={styles.linearGradient}>
                 <KeyboardAwareScrollView>
                <View style={styles.logoSpace} >
                  <ImageBackground
                  source={require('./Nap_Ios/logo.png')} style={{width: '100%', height: '100%'}}
                  resizeMode="cover"
                  blurRadius={1}/>

                  
                </View>
                <Text style={styles.headingText}>Neutral Air Partner</Text>
                <Text style={styles.head2Text} onPress={()=>Keyboard.dismiss()}>The premier global network of leading air cargo architects and aviation specialist</Text>
               
                <View style={styles.fieldsView}>
                <TextField
                  textColor={"white"}
                  keyboardType={'email-address'}
                  baseColor={"white"}
                  autoCorrect={false}
                  enablesReturnKeyAutomatically={true}
                  labelTextStyle={{color:"white", paddingLeft:'3%'}}
                  returnKeyType='next'
                  label='Email Address'
                  tintColor={'white'}
                  inputContainerStyle={{paddingLeft:'3%'}}
                  lineWidth={1}
                  onChangeText = {this.handleEmail}
                  />
         
                <TextField
                  textColor={"white"}
                  baseColor={"white"}
                  autoCorrect={false}
                  enablesReturnKeyAutomatically={true}
                  labelTextStyle={{color:"white", paddingLeft:'3%'}}
                  returnKeyType='next'
                  label='Password'
                  tintColor={'white'}
                  style={{paddingTop:'1%'}} 
                  lineWidth={1}
                  inputContainerStyle={{paddingLeft:'3%'}}
                  secureTextEntry={this.state.hidden}
                  onChangeText = {this.handlePassword}/>
                 

                <View style={styles.signInButton}>
                    <TouchableOpacity  style={{width:'100%', height: '100%'}} onPress = {() => this.login(this.state.Email, this.state.Password)}>
                        <Text style={{textAlign:'center',paddingTop:'17%',fontFamily:'GillSans-SemiBold',fontSize:15,color:'#303147',}}>SIGN IN</Text>
                    </TouchableOpacity>
                </View>
            </View>
            </KeyboardAwareScrollView>
           
                <View style={{width: wp('92%'),height:hp('5%'),alignSelf:'center',backgroundColor:'transparent',flexDirection:'row',justifyContent:'space-between'}}>
               <TouchableOpacity  onPress={()=>this.check()}
                onLongPress={()=>this.uncheck()}>
                <Text style={{color:this.state.color,fontSize:16,fontFamily:'Gill Sans'}}>{this.state.text}</Text>
                </TouchableOpacity>

                  <TouchableOpacity >
                  <Text style={{color:'white',fontSize:16,fontFamily:'Gill Sans'}}>Forgot Password</Text>
                  </TouchableOpacity>
                </View>

                <View style={{width: wp('92%'),height: hp('7%'),marginTop:hp('9%'),alignSelf:'center',backgroundColor:'transparent',justifyContent:'space-between'}}>
                  <Text style={{color:'white',fontSize:15,color:'#808080',textAlign:'center',fontFamily:'Gill Sans'}}>By clicking on above button you agree to our</Text>
                  <Text style={{color:'white' ,fontSize:16,textAlign:'center',fontFamily:'Gill Sans'}}>Terms and Conditions</Text>
                </View>
            </LinearGradient>
           
            </Transition>
            
        );
    }
}

const MyNavigator = FluidNavigator(
  {
    Login: Login,
    Home: Home,
    Filter: Filter,
    Profile: Profile,
    Header: Header,
    Promo: Promo,
    Alert: Alert,
    Geolocator: Geolocator,
    Post_Geo: Post_Geo,
    Post_Promo: Post_Promo,
    ChatScreen: ChatScreen,
    Sidebar: Sidebar,
    HeaderHome: HeaderHome
  },
  {
    header:'null',
    headerTransitionPreset: 'uikit',
     mode: 'card',
     defaultNavigationOptions: { gesturesEnabled: true },
     
  }
);






var styles = StyleSheet.create({
    linearGradient: {
      flex: 1,
      },
    logoSpace: {
      height:hp('12%'),
      width:wp('22%'),
      backgroundColor:'#303147',
      marginBottom:hp('1%'),
      marginLeft:wp('38%'),
      marginTop:hp('9%'),
      borderRadius:60
      },
    headingText:{
      fontFamily:'Montserrat-Bold',
      fontSize: hp('3.5%'),
      color:'white',
      paddingLeft: wp('22%'),
      paddingTop:hp('1.4%')
    },
    head2Text:{
      fontFamily:'Montserrat-Regular',
      fontSize: hp('2.2%'),
      color:'#808080',
      paddingTop:hp('1.4%'),
      textAlign:'center'
    },
    fieldsView:{
      width: wp('92%'),
      height: hp('40%'),
      marginTop:hp('2%'),
      alignSelf:'center',
      //backgroundColor:'green'
    },
    signInButton:{
      width: wp('29%'), 
      height: hp('9%'),
      justifyContent:'flex-start', 
      backgroundColor:'white',
      borderRadius:32,
      alignSelf:'center',
      marginTop:hp('5%'),
    },
  
   
  
  });
  export default createAppContainer(MyNavigator);