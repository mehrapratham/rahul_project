import React, { Component } from 'react';
import { View,Dimensions ,Text,StyleSheet,TouchableOpacity,Image} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { FluidNavigator, Transition } from 'react-navigation-fluid-transitions';
import Header from './Header';
import { Dropdown } from 'rn-material-dropdown';
import { ScrollView } from 'react-native-gesture-handler';
import LinearGradient from 'react-native-linear-gradient';

var {height, width} = Dimensions.get('window');
 export default class Filter extends Component{
  constructor(){
    super();
    this.state={
        layout:{
            height:height,
            width:width,
          },
    }
  }


    render() {

        let data = [{
            value: 'Country1',
          }, {
            value: 'Country2',
          }, {
            value: 'Country3',
          }];

      return(
        
          <View style={{height:this.state.layout.height, width: this.state.layout.width, backgroundColor:'transparent'}}>
            <Header/>
           
            <ScrollView style={styles.dataView}>
            
                <View style={{ height:this.state.layout.height*1.6,width:wp('100%'),alignSelf:'center',backgroundColor:'white',alignItems:'center'}}>
                   
                    {/* filter 1 */}
                    <View style={styles.container}>
                       
                        <View style={styles.fieldView}>
                        <Dropdown
                            label='Select Company'
                            lineWidth={0}
                            fontSize= {15}
                            baseColor='black'
                            textColor='black'
                            data={data}
                            containerStyle={styles.field}
                            dropdownPosition={-4}
                            pickerStyle={{marginLeft:'7%',width:'80%'}}
                        />
                        </View>

                        <View style={styles.fieldView}>
                        <Dropdown
                            label='Select Country'
                            lineWidth={0}
                            fontSize= {15}
                            baseColor='black'
                            textColor='black'
                            data={data}
                            containerStyle={styles.field}
                            dropdownPosition={-4}
                            pickerStyle={{marginLeft:'7%',width:'80%'}}
                        />
                        </View>

                        <View style={styles.fieldView}>
                        <Dropdown
                            label='Select City'
                            lineWidth={0}
                            fontSize= {15}
                            baseColor='black'
                            textColor='black'
                            data={data}
                            containerStyle={styles.field}
                            dropdownPosition={-4}
                            pickerStyle={{marginLeft:'7%',width:'80%'}}
                        />
                        </View>
                        
                        
                    </View>
                    
                    {/* airpartner start */}
                    {/* logo */}
                    <View style={styles.logoAir}>
                        <Image source={require('./Nap_Ios/destination.png')}></Image> 
                        <Text style={{color:'#332863',marginTop:'10%',marginLeft:'3%',fontSize:16,fontWeight:'800'}}>AirPartner</Text>
                    </View>

                    {/* filter 2 */}
                    <View style={styles.container2}>
                        
                    <View style={styles.fieldViewAirpartner}>
                        <Dropdown
                            label='Select Clasification'
                            lineWidth={0}
                            fontSize= {15}
                            baseColor='#f9f9f9'
                            textColor='black'
                            data={data}
                            containerStyle={styles.field}
                            dropdownPosition={-4}
                            pickerStyle={{marginLeft:'7%',width:'80%'}}
                        />
                        </View>
                        
                        <View style={styles.fieldViewAirpartner}>
                        <Dropdown
                            label='Industry Expertise'
                            lineWidth={0}
                            fontSize= {15}
                            baseColor='#f9f9f9'
                            textColor='black'
                            data={data}
                            containerStyle={styles.field}
                            dropdownPosition={-4}
                            pickerStyle={{marginLeft:'7%',width:'80%'}}
                        />
                        </View>

                        <View style={styles.fieldViewAirpartner}>
                        <Dropdown
                            label='Tradeline'
                            lineWidth={0}
                            fontSize= {15}
                            baseColor='#f9f9f9'
                            textColor='black'
                            data={data}
                            containerStyle={styles.field}
                            dropdownPosition={-4}
                            pickerStyle={{marginLeft:'7%',width:'80%'}}
                        />
                        </View>

                        <View style={styles.fieldViewAirpartner}>
                        <Dropdown
                            label='Sort By'
                            lineWidth={0}
                            fontSize= {15}
                            baseColor='#f9f9f9'
                            textColor='black'
                            data={data}
                            containerStyle={styles.field}
                            dropdownPosition={-4}
                            pickerStyle={{marginLeft:'7%',width:'80%'}}
                        />
                        </View>
                       
                    </View>
                    {/* airpartner end */}
                    
                    {/* airline start */}
                        {/* logo */}
                    <View style={styles.logoAir}>
                        <Image source={require('./Nap_Ios/airline.png')} style={{marginLeft:'4%',}}></Image> 
                        <Text style={{color:'#332863',marginTop:'10%',marginLeft:'10%',fontSize:18,fontWeight:'800'}}>Airline</Text>
                    </View>

                    {/* filter 3 */}
                    <View style={styles.container}>
                        
                    <Text style={{color:'grey',marginTop:'10%',fontSize:15,alignSelf:'center'}}>Origin Airport</Text>
                    <View style={styles.fieldViewAirline}>
                        <Dropdown
                            label='Select One Or More'
                            fontSize= {15}
                            lineWidth={0}
                            baseColor='#f9f9f9'
                            textColor='black'
                            data={data}
                            containerStyle={styles.field}
                            dropdownPosition={-4}
                            pickerStyle={{marginLeft:'7%',width:'80%'}}
                        />
                        </View>

                        <Text style={{color:'grey',marginTop:'6%',fontSize:15,alignSelf:'center'}}>Destination Airport</Text>
                        <View style={styles.fieldViewAirline}>
                        <Dropdown
                            label='Select One Or More'
                            lineWidth={0}
                            fontSize= {15}
                            baseColor='#f9f9f9'
                            textColor='black'
                            data={data}
                            containerStyle={styles.field}
                            dropdownPosition={-4}
                            pickerStyle={{marginLeft:'7%',width:'80%'}}
                        />
                        </View>
                        
                        <Text style={{color:'grey',marginTop:'6%',fontSize:15,alignSelf:'center'}}>Airline</Text>
                        <View style={styles.fieldViewAirline}>
                        <Dropdown
                            label='Select One Or More'
                            lineWidth={0}
                            fontSize= {15}
                            baseColor='#f9f9f9'
                            textColor='black'
                            data={data}
                            containerStyle={styles.field}
                            dropdownPosition={-4}
                            pickerStyle={{marginLeft:'7%',width:'80%'}}
                        />
                        </View>
                        
                    </View>
                    {/* airline end */}
                
                </View> 
                
                {/* show result button */}
                <View style={{width: this.state.layout.width, height: hp('20%'),backgroundColor:'white',marginTop:hp('2%'),justifyContent:'center'}}>
                      <TouchableOpacity style={{width:wp('50%'),height:hp('4%'),alignSelf:'center',backgroundColor:'transparent',}} onPress={()=>this.props.navigation.navigate('Home')}>
                        <LinearGradient colors={['#00cc66','#00ff99']} style={{width:'100%',height:'200%',borderRadius:45,alignItems:'center'}}
                          start={{ x: 0.1, y: 0.0 }} end={{ x: 1.2, y: 0.4 }}>
                        <Text style={{fontSize:18,fontFamily:'Gill Sans',marginTop:hp('2%'),color:'white',fontWeight:"500"}}>Send Message</Text>
                        </LinearGradient>
                       </TouchableOpacity>
                </View>
                    </ScrollView>
                    
            </View>

       
        );
    }
}


var styles = StyleSheet.create({
    container:{
        height:hp('35%'),
        width:wp('87%'),
        backgroundColor:'white',
        //alignItems:'center',
        alignSelf:'center',
        marginTop:hp('1%')
    },
    logoAir:{
        height: hp('10%'),
        width:wp('50%'),
        backgroundColor:'transparent',
        marginTop:hp('4%'),
        flexDirection:'row',
        
alignSelf:'center'
    },
    dataView:{
        height:hp('100%'),
        width:wp('100%'),
        backgroundColor:'white',
    },
   
    container2:{
        marginTop:hp('2%'),
        height:hp('45%'),
        width:wp('87%'),
        backgroundColor:'white',
        //alignItems:'center',
        alignSelf:'center'
    },

        field:{
            width:'95%',alignSelf:'flex-end',paddingLeft:'5%'
        },

        fieldView:{
            width:'100%',height:hp('7%'),backgroundColor:'#f9f9f9',flexDirection:'row',borderRadius:30,marginTop:'2%'
        },

        fieldViewAirline:{
            width:'100%',height:hp('7%'),backgroundColor:'#339966',flexDirection:'row',borderRadius:30,marginTop:'2%'
        },
        
        fieldViewAirpartner:{
            width:'100%',height:hp('7%'),backgroundColor:'#0099cc',flexDirection:'row',borderRadius:30,marginTop:'5%'
        }
  });

  



