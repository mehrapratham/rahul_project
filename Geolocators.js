import React, { Component } from 'react';
import { View,Dimensions ,Text,StyleSheet,TouchableWithoutFeedback,ImageBackground,Image,FlatList,TouchableOpacity} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { ScrollView } from 'react-native-gesture-handler';
import ElevatedView from 'react-native-elevated-view'
import { withNavigation } from 'react-navigation';
import Header from './Header';
import {Badge,ButtonGroup} from 'react-native-elements'
import { Form, Item, Picker } from 'native-base';
import { ActivityIndicator } from 'react-native-paper';
import Carousel from 'react-native-carousel-view';
import LinearGradient from 'react-native-linear-gradient';
import FAB from 'react-native-fab'


var {height, width} = Dimensions.get('window');
 class Geolocator extends Component{
    constructor(){
      super(); 
      this.state={
          layout:{
              height:height,
              width:width, 
            },
            selected2: 'key0', 
            baseURL:'http://api.neutralairpartner.com',
            array:[{'name':'vhxjsx'},{'name':'xbwqikxikqx'}],
      
              
      }  
   
    }

    onValueChange2(value) {
        this.setState({
          selected2: value
        }); 
      }
  
    render() {
        
      if (this.state.isloading) {
        return (
          <ActivityIndicator 
              animating={true} 
              color={'#303147'} 
              size={'large'}
              style={{ position:'absolute', left:0, right:0, bottom:0, top:0 }}/>
         );
       } else{
        return(
            
            <View style={{height:this.state.layout.height, width: this.state.layout.width, backgroundColor:'#f9f9f9',alignItems:"center"}}>
               <Header/>

               <ScrollView showsVerticalScrollIndicator={false} style={{height:this.state.layout.height}}>
                <Form style={{marginTop:hp('2%'), width:'95%',alignSelf:'center'}}>
                    <Item picker rounded style={{backgroundColor:'#0099cc'}}>
                        <Picker 
                            mode="dropdown"
                            style={{ width: '100%' }}
                            textStyle={{color:'white',fontFamily:'Montserrat-Bold', fontSize:13}}
                            selectedValue={this.state.selected2}
                            onValueChange={this.onValueChange2.bind(this)}>
                            <Picker.Item label="country1" value="key0" />
                            <Picker.Item label="country2" value="key1" />
                            <Picker.Item label="country3" value="key2" />
                            <Picker.Item label="country4" value="key3" />
                        </Picker>
                    </Item>
                </Form>
            
                <View style={styles.headView}>
                    <Text style={styles.headtext}>Members At Country</Text>
                </View>

                <FlatList
                data={this.state.array}
                scrollEnabled={false}  
                showsVerticalScrollIndicator={false} 
                renderItem={({ item: data, index }) => {
                return ( 
               
                <ElevatedView
                    elevation={4} 
                    style={{ width:wp('90%'),backgroundColor:'white', height:this.state.layout.height/2.5, alignSelf:'flex-start',  borderRadius:3,alignSelf:'center',marginTop:'3%',alignItems:'center'}}>

                    <View style={styles.badgeimage}>
                        <Badge status="success" containerStyle={{ position: 'absolute', top: 8, right: 61,  }}
                           badgeStyle={{ height:12,width:12,borderRadius:20}}
                           />
                    </View>

                    <View style={styles.userName}>
                        <Text style={styles.userNameText}>Brandon Smith</Text>
                        <Text style={styles.userClassification}>CEO, Kingfisher Airlines</Text>
                    </View>

                    <View style={styles.buttons}>
                        <TouchableOpacity style={styles.workButtons}>
                            <Text style={styles.buttonText}>Company</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.workButtons2}>
                            <Text style={styles.buttonText2}>Message</Text>
                        </TouchableOpacity>
                    </View>
                </ElevatedView>  
                
 
                )
                }}
                keyExtractor={(item, index) => index.toString()} /> 

                <View style={styles.headView}>
                    <Text style={styles.headtext}>Companies At Country</Text>
                </View>

                <FlatList
                data={this.state.array}
                scrollEnabled={false}  
                showsVerticalScrollIndicator={false} 
                renderItem={({ item: data, index }) => {
                return ( 
               
                <ElevatedView
                    elevation={4} 
                    style={{ width:wp('90%'),backgroundColor:'white', height:this.state.layout.height/1.2, alignSelf:'flex-start',  borderRadius:3,alignSelf:'center',marginTop:'2%',alignItems:'center'}}>

                    <View style={styles.companyHeader}>
                        <View style={{height:'60%',width:'95%',flexDirection:'row',justifyContent:'space-between',backgroundColor:'transparent',marginTop:'2%'}}>
                          <Text style={{ fontSize:18, fontFamily:'Montserrat-Light',marginTop:'2%'}}>AirPartner | NAX</Text>
                          <TouchableOpacity>
                          <Image source={require('./Nap_Ios/moreheader.png')} style={{marginTop:'60%'}}></Image>
                          </TouchableOpacity>
                        </View>

                        <View style={styles.line}></View>
                    </View>

                    <View style={styles.companyImage}>
                        <Badge status="success" containerStyle={{ position: 'absolute', top: 3, right: 63,  }}
                           badgeStyle={{ height:12,width:12,borderRadius:20}}
                           />
                    </View> 

                    <Text style={{fontSize:21, fontFamily:'Montserrat-SemiBold',color:'#332863',alignSelf:'center',marginTop:'3%'}}>Cloud Consol Ltd</Text>
                    <Text style={{fontSize:16, fontFamily:'Montserrat-Regular',color:'grey'}}>USA, Chicago</Text>
                    
                    <View style={{width:'70%',height:"16%", backgroundColor:'transparent',alignItems:'center',marginTop:'3%'}}>
                        <ImageBackground source={require('./Nap_Ios/line.png')} style={{width:'100%',height:'70%',marginTop:'7%',flexDirection:'row',justifyContent:'space-between'}}>

                        </ImageBackground>
                    </View>

                    <View style={{width:'100%',height:"25%", backgroundColor:'transparent',alignItems:'center'}}>
                        <Carousel
                        width={300}
                        scrollEnabled={false}
                        height={120}
                        delay={2000}
                        hideIndicators={true}
                        //onPageChange={this.corousel()}
                        contentContainerStyle={{backgroundColor:'transparent',}}>
                        <View style={styles.contentContainer}>
                            <Image source={require('./Nap_Ios/trade.png')}></Image>
                            <Text style={{marginTop:'3%',fontFamily:'Montserrat-Regular',fontSize:14,color:'grey'}}>Trade Lanes</Text>
                            <Text style={{marginTop:'1%',fontFamily:'Montserrat-Regular',fontSize:18,}}>Oceania, North America</Text>
                            <Text style={{marginTop:'1%',fontFamily:'Montserrat-Regular',fontSize:18,}}>Middle Asia</Text>
                        </View>
                        <View style={styles.contentContainer}>
                        <Image source={require('./Nap_Ios/special.png')} ></Image>
                            <Text style={{marginTop:'3%',fontFamily:'Montserrat-Regular',fontSize:14,color:'grey'}}>Specializations</Text>
                            <Text style={{marginTop:'1%',fontFamily:'Montserrat-Regular',fontSize:18,}}>Time Critical, EComerce</Text>
                            <Text style={{marginTop:'1%',fontFamily:'Montserrat-Regular',fontSize:18,}}>Neutral Air Frieght</Text>
                        </View>
                        <View style={styles.contentContainer}>
                        <Image source={require('./Nap_Ios/branches.png')} ></Image>
                            <Text style={{marginTop:'3%',fontFamily:'Montserrat-Regular',fontSize:14,color:'grey'}}>Branches</Text>
                            <Text style={{marginTop:'1%',fontFamily:'Montserrat-Regular',fontSize:18,}}>Paris, Dakhar</Text>
                            <Text style={{marginTop:'1%',fontFamily:'Montserrat-Regular',fontSize:18,}}>Dubai</Text>
                        </View>
                        </Carousel>
     
                    </View> 
                </ElevatedView>  
                )
                }}
                keyExtractor={(item, index) => index.toString()} /> 
                </ScrollView>
                <FAB buttonColor="transparent" onClickAction={() => this.props.navigation.navigate('Post_Geo')} visible={true} iconTextComponent={<Image source={require('./Nap_Ios/createnwew.png')}/>} />
             </View>
             

            
          );
      }}
  }
  
  
  var styles = StyleSheet.create({
   
    badgeimage:{
        height:'28%',
        width:'23%',
        marginTop:'3%',
        borderRadius:45,
        backgroundColor:'#2B303A'
    },
    userName:{
        height:'25%',
        width:'90%',
        backgroundColor:'transparent',
        alignItems:'center',marginTop:'2%'
    },
    userNameText:{
        fontFamily:'Montserrat-Regular',
        fontSize:22,
        fontWeight:'500',
        color:'#332863'
    },

    userClassification:{
        fontFamily:'Montserrat-Light',
        fontSize:12,
        color:'grey'
    },

    buttons:{
        height:'25%',
        width:'90%',
        backgroundColor:'transparent',
        alignItems:'center',
        marginTop:'3%',
        flexDirection:'row',
        justifyContent:'center'
    },
    workButtons:{
        height:'60%',width:'30%',
        backgroundColor:'#2B303A',
        borderBottomLeftRadius:20,
        borderTopLeftRadius:20,
        alignItems:'center'
    },
    workButtons2:{
        height:'60%',width:'30%',
        backgroundColor:'#C1DBE3',
        borderBottomRightRadius:20,
        borderTopRightRadius:20,
        alignItems:'center'
    },
    buttonText:{
        fontFamily:'Montserrat-Regular',
        fontSize:13, color:'white',
        marginTop:'11%'

    },
    buttonText2:{
        fontFamily:'Montserrat-Regular',
        fontSize:13, color:'black',
        marginTop:'11%'
    },
    headView:{
        height:'3%',
        width:wp('90%'),
        backgroundColor:'transparent', 
       flexDirection:'row',
        alignSelf:'center'
    },
    headtext:{
        fontSize:18,
      fontFamily:'Montserrat-Light',
      color:'black',
      marginTop:'2%',
      alignSelf:'center'
    },
    companyImage:{
        height:'15%',
        width:'25%',
        marginTop:'6%',
        borderRadius:45,
        backgroundColor:'#2B303A'
    },
    companyHeader:{
        height:'13%',
        width:'100%',
        backgroundColor:'transparent',
        alignItems:'center'
    },
    line:{
        height:'0.7%',
        width:'100%',
        backgroundColor:'grey'
    }, 
    contentContainer: {
        borderColor: 'white',
        alignItems: 'center',
        backgroundColor:'transparent',
        height:'100%'
      },
      floatAdd:{
          height:hp('20%'),
          width:wp('100%'),
          backgroundColor:'transparent',
          alignItems:'flex-end',
          alignSelf:'flex-end',
          flexDirection:'column',
      }

    
    });

    export default withNavigation(Geolocator);





  