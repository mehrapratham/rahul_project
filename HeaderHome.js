import React, { Component } from 'react';
import { View,Dimensions ,Text,StyleSheet,TouchableWithoutFeedback,Image,ImageBackground,TouchableOpacity} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { Avatar,Icon,} from 'react-native-elements';
import ElevatedView from 'react-native-elevated-view'
import { withNavigation, } from 'react-navigation';

import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';


const {width}= Dimensions.get('screen')
 class HeaderHome extends Component{
    constructor(){
      super();
      this.state={
        results:[],
        load: false
        }
        
    }  
        render() {
           return(
        
            <ElevatedView
                elevation={1}
                style={{height:hp('12%'),width:wp('100%'),backgroundColor:'white', flexDirection:'row',justifyContent:'space-between'}}>
                
                {/* left Icons */}
                <View style={{height:'75%',width:wp('30%'),backgroundColor:'transparent',flexDirection:'row',justifyContent:'space-evenly',marginTop:'5%'}}>
                <OrientationLoadingOverlay
                          visible={this.state.load}
                          color="#303147"
                          loader='ScaleLoader'
                          indicatorSize="large"
                          messageFontSize={14}
                          style={{
                            position:'absolute', left:0, right:0, bottom:0, top:0 }}>
                        </OrientationLoadingOverlay>
                  <View style={{height:'50%',width:'35%',backgroundColor:'transparent',marginTop:hp('2.5%')}}>
                    <TouchableWithoutFeedback style={{height:'100%',width:'100%'}}>
                    <ImageBackground style={{height:'100%',width:'100%'}} source={require('./Nap_Ios/logo.png')}>
                    </ImageBackground>
                    </TouchableWithoutFeedback>
                  </View>
                  
                  <TouchableOpacity onPress={()=>this.props.navigation.toggleDrawer()}>
                    <Image source={require('./Nap_Ios/menu.png')} style={{marginTop:hp('2.5%'),height:hp('5%'),width:wp('8%')}}></Image>
                  </TouchableOpacity>
                  </View>
                
             
                
                {/* rightIcons */}
                <View style={{height:'75%',width:wp('30%'),backgroundColor:'transparent',flexDirection:'row',justifyContent:'space-evenly',marginTop:'5%'}}>
                    

                    <Avatar
                      rounded
                      size='small'
                      containerStyle={{marginTop:hp('2.5%')}}
                      source={require('./Nap_Ios/a2.png')}
                     
                    />
                <TouchableWithoutFeedback onPress={()=>this.props.navigation.navigate('Sidebar')}>
                  <Image source={require('./Nap_Ios/notification.png')} style={{marginTop:hp('4%'),height:hp('3%'),width:wp('6%')}}></Image>
                </TouchableWithoutFeedback>
                </View>

              
                      
             </ElevatedView>
         
          );
      }
  }



  export default withNavigation(HeaderHome)