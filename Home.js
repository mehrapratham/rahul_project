import React, { Component } from 'react';
import { View,Dimensions ,Text,StyleSheet,TouchableOpacity,ScrollView,ImageBackground,AsyncStorage,Image,FlatList,SafeAreaView} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import HeaderHome from './HeaderHome';


import LinearGradient from 'react-native-linear-gradient';
import { Card,Tooltip } from 'react-native-elements';
import Carousel from 'react-native-carousel-view';
import { AnimatedCircularProgress } from 'react-native-circular-progress';
import { Transition } from 'react-navigation-fluid-transitions';

import { withNavigation,createDrawerNavigator,createAppContainer} from 'react-navigation';
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';
import Sidebar from './Sidebar'
import {OptimizedFlatList} from 'react-native-optimized-flatlist'




var {height, width} = Dimensions.get('window');
 class Home extends Component{
    constructor(){
      super();
      global.id=0
      global.tradelanes=''
      //global.animation=false
      this.state={
          layout:{
          height:height,
          width:width,
         
        },
        fill:50,
        api:'/v1/network',
        CompanyData:[],
        baseURL:'http://api.neutralairpartner.com',
       
        isrefreshing: false,
        countryName:'loading..',
        isloading:true,
        page:1,
        array:['One'],
        animation: false, 
        
      }
    }
    
     async onFetchCompanyRecords(token, page) {
      try {
       let response = await fetch(
        'http://api.neutralairpartner.com/v1/network?page='+page,
        {
          'method': 'GET',
          headers: {
            'Accept': 'application/json',
           'Content-Type': 'application/json',
           'Authorization':'Bearer'+' '+token
           },
       }
      );
       if (response.status >= 200 && response.status < 300) {
         response.json().then(data => {
            data.map((item,key)=>{
              this.state.CompanyData.push(item)
             
              })
              
          }); 
          this.setState({isloading:false, isrefreshing: false})
        } else{
          alert('JWT token not found')
        }
     } catch (errors) {
        alert(errors);
       } 
  }



  handleRefresh = () => {
    this.setState({
     isRefreshing: true,
    }, () => {
      this.onFetchCompanyRecords(global.token, this.state.page);
      
    });
  };


  renderFooter = () => {
    //it will show indicator at the bottom of the list when data is loading otherwise it returns null
     if (!this.state.isrefreshing) return null;
     return (
       <ActivityIndicator
         style={{ color: '#000' }}
       />
     );
   };

  //for more
  handleLoadMore=()=>{
    
    this.setState({
      page: this.state.page + 1
    }, () => {
      this.onFetchCompanyRecords(global.token,this.state.page);
      
    });
    
  
  };
  
  navigate=()=>{
    this.props.navigation.navigate('Alert')
  }

 
  //for detail screen
  viewprofile(id){
  
    global.id= id
  
    this.props.navigation.navigate('Profile',{callHome: this.onback.bind(this)})
 
  }
  
  onback(){
    try{
       this.setState({animation: false})
     
    }catch(errors){
        alert(errors)
      }
       //this.onFetchOccasionRecords()
  }



componentDidMount=()=>{
     
     this.onFetchCompanyRecords(global.token, this.state.page )
   
    }

  

    render() {
      if (this.state.isloading) {
        return (
          <OrientationLoadingOverlay
          visible={true}
                  color="#303147"
                  loader='ScaleLoader'
                  indicatorSize="large"
                  messageFontSize={14}
                  style={{
                    position:'absolute', left:0, right:0, bottom:0, top:0 }}>
        </OrientationLoadingOverlay>
         );
       } else{
       return(
       
        <Transition appear='horizontal' >
            <View style={styles.container}>
                <HeaderHome/>
                <FlatList
                  
                  data={this.state.array} 
                  refreshing={this.state.isrefreshing}
                  onRefresh={this.handleRefresh}
                  renderItem={({ item: data, index }) => { 
                return (
                    <View style={{ height:'auto',width:wp('100%'),alignSelf:'center',backgroundColor:'#f9f9f9',alignItems:'center'}}>

                    {/* heading */}
                    
                    <View style={styles.heading}>

                      <Text style={styles.text}>Network</Text>
                        
                      <TouchableOpacity style={styles.button} onPress={()=>this.props.navigation.navigate('Filter')}>
                        <LinearGradient colors={['#00cc66','#00ff99']} style={styles.buttonView}
                          start={{ x: 0.1, y: 0.0 }} end={{ x: 1.2, y: 0.4 }}>
                        <Text style={styles.buttonText}>Filter</Text>
                        </LinearGradient>
                      </TouchableOpacity>
                       
                    </View>

                  {/* profileReadiness */}
                    <View style={styles.profileReadiness}>
                      <LinearGradient colors={['#6666ff','#66ffff']} style={{height:'100%',width:'100%',alignItems:'center'}}
                        start={{ x: 0.0, y: 0.0 }} end={{ x: 1.4, y: 0.4 }}>
                       <ImageBackground
                          source={require('./Nap_Ios/circles.png')} style={{width: '100%', height: '100%'}}>
                        <View style={{ height:'100%', width:'100%',backgroundColor:'transparent',justifyContent:'flex-start'}}>
                          <View style={{ height:'20%', width:'90%',backgroundColor:'transparent',justifyContent:'space-between',flexDirection:'row',alignSelf:'center'}}>
                          <Text style={{marginTop:'5%',fontSize:17,color:'white',fontWeight:'600',fontFamily:'Montserrat-Regular'}}>Profile Readiness</Text>
                          <Text style={{marginTop:'6%',fontSize:14,color:'white',fontWeight:'600',fontFamily:'Montserrat-Regular'}}>Whats Left+</Text>
                        </View>

                         
                          <View style={{ height:'80%', width:'90%',backgroundColor:'transparent',alignSelf:'center',alignItems:'center',paddingTop:wp('3%')}}>
                          
                          <AnimatedCircularProgress
                            size={110}
                            width={4}
                            fill={this.state.fill}
                            tintColor="white"
                            rotation={360}
                            backgroundColor="#ADD8E6">

                                      {
                                          (fill) => (
                                            <Text style={styles.points}>
                                              { this.state.fill }%
                                            </Text>
                                          )
                                        }
                            
                            </AnimatedCircularProgress>
                          </View>
                        </View>
                        </ImageBackground>                   
                      </LinearGradient>
                    </View>
                 

                  {/* company Info */}
                  <View style={{height:'100%',width:'100%',backgroundColor:'transparent'}}>
                  <FlatList
                    data={this.state.CompanyData}
                    ListFooterComponent={this.renderFooter}
                    extraData={this.state}
                    onEndReached={this.handleLoadMore}
                    onEndReachedThreshold={0.3}
                    renderItem={({ item: data, index }) => { 
                      
                      let name='' 
                      let url=require('./Nap_Ios/a2.png')
                      let country=''

                    ///////countryName/////
                      if(data.countries!=null){
                        country= data.countries.countryname
                      }else{
                        country= 'N/A'
                      }

                    /////////// Trade////////
                      let tradeText='Trade Lanes'
                      let tradeHeight=35
                      let tradeWidth=35
                      let tradeFont=14
                      let tradeUrl= require('./Nap_Ios/destination.png')
                      if(data.airIndustryExpertise.length>0){
                        tradeUrl= require('./Nap_Ios/trade.png')
                        tradeText='Trade Lanes'
                        if(data.airIndustryExpertise.length>=3){
                          tradeFont=13
                        }
                        
                      }else{
                        tradeUrl= require('./Nap_Ios/destination.png')
                        tradeText=''
                        tradeHeight=60
                        tradeWidth= 60
                      }


                       ///////////Branch////////
                      let branchText= 'Branches'
                      let branch= require('./Nap_Ios/destination.png')
                      let branchHeight=35
                      let branchWidth=35
                      let branchFont=14
                      if(data.branch.length>0){
                        branchText='Branches'
                        branch= require('./Nap_Ios/special.png')

                        if(data.branch.length>=3){
                          branchFont=13
                        }
                      }else{
                        branch= require('./Nap_Ios/destination.png')
                        branchText=''
                        branchHeight=60
                        branchWidth= 60
                      }


                       ///////////Special////////
                      let specialText= 'Specializations'
                      let special= require('./Nap_Ios/destination.png')
                      let specialHeight=35
                      let specialWidth=35
                      let specialFont=14
                      if(data.specializations.length>0){
                        specialText= 'Specializations'
                        special= require('./Nap_Ios/branches.png')
                        if(data.specializations.length>=5){
                          specialFont=13
                        }
                      }else{
                        special= require('./Nap_Ios/destination.png')
                        specialText= ''
                        specialHeight=60
                        specialWidth= 60
                      }

                    ///////companyName///////
                      name=data.companyName
                      if(name!==null){
                        name= name.toUpperCase()
                      }else{
                          name= 'loading..'
                      }

                    ////////Logo/////////////
                      if(data.logo.length > 0){
                         url={uri: data.logo[0].path}
                       } else{
                         url=require('./Nap_Ios/a2.png')
                       }
                     
  
                    return ( 
                    <Card containerStyle={{ width:'100%',backgroundColor:'white',marginTop:hp('4%'), height:this.state.layout.height*1.1, alignSelf:'center', borderRadius:3}} >

                        <View style={{height:hp('7%'),width:'100%',backgroundColor:'white', flexDirection:'row',justifyContent:'space-between',alignItems:'flex-start',margin:0}}>
                          <Text style={{ fontSize:20, fontFamily:'Montserrat-Light'}}>{data.companySettings[0].classification}</Text>
                          <TouchableOpacity>
                          <Image source={require('./Nap_Ios/moreheader.png')} style={{marginTop:hp("1%")}} ></Image>
                          </TouchableOpacity>
                        </View>

                        <View style={{height:"0.3%",width:'100%',backgroundColor:'#D3D3D3',alignSelf:'stretch'}}></View>
                        
                        <View style={{width:'100%', height:hp('45%'), backgroundColor:'white', alignItems:'center', marginTop:hp('3%')}}>


                            <Image
                              style={{width:80, height:80,backgroundColor:'white',borderRadius:80/2}}
                              resizeMode={'contain'} 
                              source={url}></Image>
                         
                          <Text style={{fontSize:15, fontFamily:'Montserrat-SemiBold',color:'#332863',alignSelf:'center',width:'auto',height:"auto",textAlign: "center",marginTop:'3%'}}>{name}</Text>
                          <Text style={{fontSize:18, fontFamily:'Montserrat-Regular',color:'grey'}}>{country}</Text>
                            
                          <View style={{width:'70%',height:"40%", backgroundColor:'white',alignItems:'center',marginTop:'7%'}}>
                            <Text style={{fontSize:18, fontFamily:'Montserrat-Light',color:'grey',marginTop:'7%'}}>Sales Focus</Text>
                            
                            <View style={{width:'100%',height:'72%', backgroundColor:'white', alignItems:'center',flexDirection:'row',}}>
                              <ImageBackground source={require('./Nap_Ios/line.png')} style={{width:'100%',height:'70%',marginTop:'7%',flexDirection:'row',justifyContent:'space-between'}}>
                                  
                                  <View style={{width:'10%',height:'27%',backgroundColor:'white',marginTop:'1%'}}>
                                  <Tooltip popover={
                                  <View>
                                  <Text style={styles.curveDataHead}>EXPORTS</Text>
                                  <Text style={styles.curveDataInfo}>Percent:{data.companyData[0].focusExports}</Text>
                                  </View>}
                                    withOverlay={false}
                                    backgroundColor={'#D3D3D3'}
                                    width={100}
                                    height={50}>
                                       <View style={{width:'100%',height:'100%',borderRadius:70,backgroundColor:'#3399ff',alignItems:'center',borderColor:'#0099ff'}}>
                                      <View style={{width:'75%',height:'72%',backgroundColor:'white',marginTop:'15%',borderRadius:70}}></View>
                                      </View>
                                  </Tooltip>
                                  </View>

                                  <View style={{width:'10%',height:'15%',backgroundColor:'white',marginTop:'14%'}}>
                                  <Tooltip popover={
                                  <View>
                                  <Text style={styles.curveDataHead}>IMPORTS</Text>
                                  <Text style={styles.curveDataInfo}>Percent:{data.companyData[0].focusImports}</Text>
                                  </View>
                                  }
                                    withOverlay={false}
                                    backgroundColor={'#D3D3D3'}
                                    width={100}
                                    height={50}>
                                      <View style={{width:'100%',height:'85%',borderRadius:70,backgroundColor:'#0099ff',alignItems:'center',borderColor:'#0099ff'}}>
                                      <View style={{width:'75%',height:'72%',backgroundColor:'white',marginTop:'15%',borderRadius:70}}></View>
                                      </View>
                                  </Tooltip>
                                  </View>

                                  <View style={{width:'10%',height:'27%',backgroundColor:'white'}}>
                                  <Tooltip popover={
                                    <View>
                                    <Text style={styles.curveDataHead}>CROSS-TRADE</Text>
                                    <Text style={styles.curveDataInfo}>Percent:{data.companyData[0].focusCrossTrade}</Text>
                                    </View>}
                                    withOverlay={false}
                                    backgroundColor={'#D3D3D3'}
                                    width={120}
                                    height={50}>
                                      <View style={{width:'100%',height:'100%',borderRadius:72,backgroundColor:'#0099ff',alignItems:'center',borderColor:'#0099ff'}}>
                                      <View style={{width:'73%',height:'75%',backgroundColor:'white',marginTop:'12%',borderRadius:70}}></View>
                                      </View>
                                  </Tooltip>
                                  </View>

                                  <View style={{width:'10%',height:'12%',backgroundColor:'white',marginTop:'16%'}}>
                                  <Tooltip popover={
                                      <View>
                                      <Text style={styles.curveDataHead}>E-COMMERCE</Text>
                                      <Text style={styles.curveDataInfo}>Percent:{data.companyData[0].focusEcommerce}</Text>
                                      </View>
                                          }
                                    withOverlay={false}
                                    backgroundColor={'#D3D3D3'}
                                    width={120}
                                    height={50}>
                                      <View style={{width:'100%',height:'85%',borderRadius:70,backgroundColor:'#0099ff',alignItems:'center',borderColor:'#0099ff'}}>
                                      <View style={{width:'73%',height:'75%',backgroundColor:'white',marginTop:'12%',borderRadius:70}}></View>
                                      </View>
                                  </Tooltip>
                                  </View>

                              </ImageBackground>
                            </View>
                          </View>
                          
                        </View>

                        {/* Company Data Slides */}
                        <View style={{width:'100%',height:'auto', backgroundColor:'transparent',alignItems:'center',marginTop:hp('10%')}}>
                        <Carousel
                        scrollEnabled={false}
                        width={315}
                        height={150}
                        delay={2000}
                        hideIndicators={true}
                        indicatorAtBottom={true}
                        indicatorSize={20}
                        indicatorText="."
                        inactiveIndicatorText='o'
                        inactiveIndicatorColor='#332863'
                        indicatorColor="#303147"
                        //onPageChange={this.corousel()}
                        contentContainerStyle={{justifyContent:'flex-start',flex:1}}>
                        <View style={styles.contentContainer}>
                            <Image source={tradeUrl} style={{height:tradeHeight, width:tradeWidth}}></Image>
                            <Text style={{marginTop:'2%',fontFamily:'Montserrat-Regular',fontSize:14,color:'grey'}}>{tradeText}</Text>
                            <OptimizedFlatList
                               data={data.airIndustryExpertise}
                               scrollEnabled={false}
                               numColumns={1}
                               style={{alignItems:'center'}}
                              renderItem={({ item: data }) => {
                                
                                return (
                                <Text style={{fontFamily:'Montserrat-Regular',fontSize:tradeFont,textAlign:'center'}}>{data.industryExpertise} </Text>
                            
                                )}}
                                listKey={(item, index) => 'D' + index.toString()}
                                keyExtractor={(item, index) => index.toString()} /> 
                        </View>
                        
                        <View style={styles.contentContainer}>
                        <Image source={special} style={{height:specialHeight, width:specialWidth}}></Image>
                            <Text style={{marginTop:'2%',fontFamily:'Montserrat-Regular',fontSize:14,color:'grey'}}>{specialText}</Text>
                            <OptimizedFlatList
                               data={data.specializations}
                               scrollEnabled={false}
                               numColumns={2}
                               style={{alignItems:'center'}}
                              renderItem={({ item: dataSpecial }) => {
                                
                                return (
                                <Text style={{fontFamily:'Montserrat-Regular',fontSize:specialFont,textAlign:'center'}}>{dataSpecial.specialization} </Text>
                            
                                )}}
                                listKey={(item, index) => 'D' + index.toString()}
                                keyExtractor={(item, index) => index.toString()} /> 
                           
                        </View>
                        
                        <View style={styles.contentContainer}>
                        <Image source={branch} style={{height:branchHeight, width:branchWidth}}></Image>
                            <Text style={{marginTop:'3%',fontFamily:'Montserrat-Regular',fontSize:14,color:'grey'}}>{branchText}</Text>
                           
                            <OptimizedFlatList
                               data={data.branch}
                               scrollEnabled={false}
                               numColumns={2}
                               style={{alignItems:'center'}}
                              renderItem={({ item: data }) => {
                                return (
                                <Text style={{fontFamily:'Montserrat-Regular',fontSize:branchFont,textAlign:'center'}}>{data.branchCity.slice(0,10)},</Text>
                                )}}
                                listKey={(item, index) => 'D' + index.toString()}
                                keyExtractor={(item, index) => index.toString()} />
                            
                            
                        </View>
                        </Carousel>
                        </View>
                            
                        
                        <View style={styles.viewprofile}>
                        <LinearGradient colors={['#00cc66','#00ff99']} style={{width:'100%',height:'90%',borderRadius:45,alignItems:'center'}}
                          start={{ x: 0.1, y: 0.0 }} end={{ x: 1.2, y: 0.4 }}>
                          <TouchableOpacity style={{height:'100%',width:'100%',alignItems:'center'}}  onPress={()=> this.viewprofile(data.id)}> 
                          <Text style={{fontSize:hp('3%'),fontFamily:'Montserrat-Regular',marginTop:hp('1.7%'),color:'white'}}>View Profile</Text>
                          </TouchableOpacity>
                        </LinearGradient>
                        </View>
                      
               
                      </Card>
                           )
                    }}
                      keyExtractor={(item, index) => index.toString()} style={{ width:'100%', height: 'auto'}}/>
                      </View>
                    
                      
                      
                  </View>
                    )
                  }}
                    keyExtractor={(item, index) => index.toString()} style={{ width:'100%', height: 'auto'}}/>
              
                  {/* ss */}
                  
                </View>
                </Transition>
                
            );
          } 
      }
  }


  
  const AppDrawerNavigator= createDrawerNavigator({
  
    Home: Home,
    // Promo: Promo,
    // Alert: Alert,
    // Geolocator: Geolocator,
    
  },{
    contentComponent: props => <Sidebar />,
    drawerWidth: width/4,
    drawerBackgroundColor:'#303147',
   
    animation:false,
    
  })
const AppContainer= createAppContainer(AppDrawerNavigator)

  var styles = StyleSheet.create({
    container: {
      flex: 1,
      height:hp('100%'),
      width:wp('100%'),
      justifyContent: 'flex-start',
      alignItems: 'stretch',
    },
    dataView:{
        height:'auto',
        width:wp('100%'),
        alignSelf:'center',
        backgroundColor:'#D3D3D3',
        
    },
    heading:{
      height:hp('10%'),
      width: '92%',
      marginTop:hp('2%'),
      //backgroundColor:'red',
      flexDirection:'row',
      justifyContent:'space-between',
      alignItems:'center',
      alignSelf:'center'
    },
    text:{
      fontSize:20,
      fontFamily:'Montserrat-Light',
    },
    button:{
      width:wp('23%'),
      height: hp('6%'),
    },
    buttonView:{
      width:'100%',
      height:'100%',
      //backgroundColor:'green',
      borderRadius:30,
      alignItems:'center'
    },
    buttonText:{
      fontSize:18,
      fontFamily:'Montserrat-Regular',
      marginTop:hp('1.1%'),
      color:'white',
     
    },
    profileReadiness:{
      width:'92%',
      height: hp('32%'),
      alignSelf:'center'
      //backgroundColor:'blue',
    },
   
    contentContainer: {
      borderColor: 'white',
      justifyContent: 'center',
      alignItems: 'center',    
    },
    points:{
      color:'white',
      fontFamily:'Gill Sans',
      fontSize: 18
    },

    viewprofile:{
      width:'50%',height:'10%',alignSelf:'center',backgroundColor:'white',marginTop:'4%'
    },
    
    curveDataHead:{
      color:'white',
      fontFamily:'Montserrat-SemiBold',
      fontSize:13,

    },
    curveDataInfo:{
      color:'white',
      fontFamily:'Montserrat-Regular',
      fontSize:11,
      
    }
    
  });

   export default withNavigation(AppContainer);

  //  onPress={()=>this.viewprofile(data.id)}