import React, { Component } from 'react';
import { View,Dimensions ,Text,StyleSheet,TouchableWithoutFeedback,ImageBackground,Image,FlatList,TouchableOpacity} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { ScrollView } from 'react-native-gesture-handler';
import ElevatedView from 'react-native-elevated-view'
import { withNavigation } from 'react-navigation';
import Header from './Header';
import LinearGradient from 'react-native-linear-gradient';
import { Form, Item, Picker } from 'native-base';


var {height, width} = Dimensions.get('window');
 class Menu extends Component{
    constructor(){
      super();
      this.state={
          layout:{
              height:height,
              width:width,
            },
            baseURL:'http://api.neutralairpartner.com',
            casApi:'/v1/cas',
            selected2: 'key0', 
            array:[{'name':'bnixu'},{'name':'hvjvjk'}]     
      } 
    }


  
    render() {
        return(
          
            <View style={{height:this.state.layout.height, width: this.state.layout.width, backgroundColor:'#f9f9f9',alignItems:"center"}}>
               
               
             </View>

            
          );
      }
  }
  
  
  var styles = StyleSheet.create({
   
    AlertHead:{
      height:'85%',
      width:'90%',
      backgroundColor:'transparent',
      marginTop:'5%'
    },
    AlertCompanyName:{
      height:'10%',
      width:'95%',
      backgroundColor:'transparent',
      alignSelf:'center',
      flexDirection:'row',
      justifyContent:'space-between'
    },
    HeadText:{
      fontFamily:'Montserrat-Regular',
      fontSize:13,
      color:'black'
    },
    HeadSecondaryText:{
      fontFamily:'Montserrat-Regular',
      fontSize:11,
      color:'black'
    },
    AlertDocument:{
      height:'12%',
      width:'95%',
      backgroundColor:'transparent',
      alignSelf:'center',
      marginTop:'3%'
    },

    AlertDownloadsView:{
      height:'40%',
      width:'95%',
      backgroundColor:'transparent',
      marginTop:'3%',
      alignSelf:'center',
      justifyContent:'space-around'
    },
    DownloadText:{
      fontFamily:'Montserrat-Light',
      fontSize:12,
      color:'black'
    },
    Boldtext:{
      fontFamily:'Montserrat-Bold',
      fontSize:11,
      color:'black'
    }
  
    });

    export default withNavigation(Menu);