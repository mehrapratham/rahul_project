import React, { Component } from 'react';
import { View,Dimensions ,Text,StyleSheet,TouchableWithoutFeedback,ImageBackground,Image,FlatList,TouchableOpacity} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { ScrollView } from 'react-native-gesture-handler';
import ElevatedView from 'react-native-elevated-view'
import { withNavigation } from 'react-navigation';
import Header from './Header';
import LinearGradient from 'react-native-linear-gradient';
import { Form, Item, Picker,Input,Label } from 'native-base';
import { ActivityIndicator } from 'react-native-paper';

import { DatePicker } from 'react-native-woodpicker';



var {height, width} = Dimensions.get('window');
 class Post_Geo extends Component{ 
    constructor(){
      super(); 
      this.state={
          layout:{
              height:height,
              width:width, 
            },
            baseURL:'http://api.neutralairpartner.com',
            Startdate: null ,
            Enddate: null,
            Title:'',   
            OriginSelected:'',
            DestinationSelected:''
      } 
    }

    handleDatePicker = data => {
        this.setState({ Startdate: data });
      };

      handleDatePicker2 = data => {
        this.setState({ Enddate: data });
      };

      handleTitle=(text)=>{
        this.setState({Title:(text)})
      } 

      handlePlaceholder = () =>
      this.state.Startdate
        ? this.state.Startdate.toDateString()
        : "No value Selected";

        handlePlaceholder2 = () =>
        this.state.Enddate
          ? this.state.Enddate.toDateString()
          : "No value Selected";

          onOriginValueChange(value) {
            this.setState({
              OriginSelected: value
            });
          }

          onDestinationValueChange(value) {
            this.setState({
              DestinationSelected: value
            });
          }


    render() {
      if (this.state.isloading) {
        return (
          <ActivityIndicator 
              animating={true} 
              color={'#303147'} 
              size={'large'}
              style={{ position:'absolute', left:0, right:0, bottom:0, top:0 }}/>
         );
       } else{
        return(
            <View style={{height:this.state.layout.height, width: this.state.layout.width, backgroundColor:'#f9f9f9',alignItems:"center"}}>
                <Header/>

                <ElevatedView
                    elevation={2} 
                    style={{ width:wp('90%'),backgroundColor:'white', height:this.state.layout.height/1.5, alignSelf:'flex-start',  borderRadius:3,alignSelf:'center',marginTop:'8%',alignItems:'center'}}>
                    
                    <View style={styles.geoIcon}>
                        <Image source={require('./Nap_Ios/geo.png')} style={{height:'61%',width:'23%', marginTop:'8%'}}></Image>
                    </View>

                    <View style={styles.fieldView}>
                    {/* Startdate */}
                    <Text style={styles.label}>Select From Date</Text>
                    <View style={{flexDirection:'row',backgroundColor:'transparent',width:'90%',height:'13%',marginTop:'2%',justifyContent:'space-between'}}>
                        <DatePicker
                        style={styles.datepicker}
                        onDateChange={this.handleDatePicker}
                        value={this.state.Startdate}
                        title="Date Picker"
                        placeholderStyle={{color:'grey',fontSize:13,fontFamily:'Montserrat-Light'}}
                        placeholder={this.handlePlaceholder()}
                        iosPickerMode="date"
                        androidPickerMode="spinner"
                        />
                        <Image source={require('./Nap_Ios/calender.png')} style={{width:'5%',height:'40%',marginTop:'3%',}}/>
                    </View>
                    <View  style={{width:'95%',height:'0.3%' ,backgroundColor:'#D3D3D3'}}/>  
                    {/* date */}

                    {/* Expirydate */}
                    <Text style={styles.label}>Select To Date</Text>
                    <View style={{flexDirection:'row',backgroundColor:'transparent',width:'90%',height:'13%',marginTop:'2%',justifyContent:'space-between'}}>
                        <DatePicker
                        style={styles.datepicker}
                        onDateChange={this.handleDatePicker2}
                        value={this.state.Enddate}
                        title="Date Picker"
                        placeholderStyle={{color:'grey',fontSize:13,fontFamily:'Montserrat-Light'}}
                        placeholder={this.handlePlaceholder2()}
                        iosPickerMode="date"
                        androidPickerMode="spinner"
                        />
                        <Image source={require('./Nap_Ios/calender.png')} style={{width:'5%',height:'40%',marginTop:'3%',}}/>
                    </View>
                    <View  style={{width:'95%',height:'0.3%' ,backgroundColor:'#D3D3D3'}}/>  
                    {/* date */}

                     {/* Country */}
                    <Text style={styles.label}>Select Country</Text>
                    <View style={{flexDirection:'row',backgroundColor:'transparent',width:'90%',height:'13%',marginTop:'1%',justifyContent:'space-around'}}>
                    <Form >
                    <Item picker style={{backgroundColor:'transparent',width:'100%'}}>
                    <Picker
                        mode="dropdown"
                        style={{width:'100%'}}
                        textStyle={{color:'grey',fontFamily:'Montserrat-Light', fontSize:13}}
                        placeholder="Nothing Selected"
                        placeholderStyle={{ color: 'grey',fontSize:13,fontFamily:'Montserrat-Light' }}
                        selectedValue={this.state.OriginSelected}
                        onValueChange={this.onOriginValueChange.bind(this)}>
                        <Picker.Item label="Origin1" value="key0" />
                        <Picker.Item label="Origin2" value="key1" />
                        <Picker.Item label="Origin3" value="key2" />
                        <Picker.Item label="Origin4" value="key3" />
                        <Picker.Item label="Origin5" value="key4" />
                    </Picker>
                    </Item>
                    </Form>
                        <Image source={require('./Nap_Ios/pin.png')} style={{width:'5%',height:'50%',marginTop:'3%',}}/>
                    </View>
                    
                    {/* date */}

                     {/* City */}
                     <Text style={styles.label}>Select City</Text>
                    <View style={{flexDirection:'row',backgroundColor:'transparent',width:'90%',height:'13%',justifyContent:'space-around'}}>
                    <Form >
                    <Item picker  style={{backgroundColor:'transparent',width:'100%'}}>
                    <Picker
                        mode="dropdown"
                        style={{width:'100%'}}
                        textStyle={{color:'grey',fontFamily:'Montserrat-Light', fontSize:13}}
                        placeholder="Nothing Selected"
                        placeholderStyle={{ color: 'grey',fontSize:13,fontFamily:'Montserrat-Light' }}
                        selectedValue={this.state.DestinationSelected}
                        onValueChange={this.onDestinationValueChange.bind(this)}>
                        <Picker.Item label="Destination1" value="key0" />
                        <Picker.Item label="Destination2" value="key1" />
                        <Picker.Item label="Destination3" value="key2" />
                        <Picker.Item label="Destination4" value="key3" />
                        <Picker.Item label="Destination5" value="key4" />
                    </Picker>
                    </Item>
                    </Form>
                        <Image source={require('./Nap_Ios/pin.png')} style={{width:'5%',height:'50%',marginTop:'3%',}}/>
                    </View>
                    
                    {/* date */}


                    </View>
                    </ElevatedView>

                    <View style={styles.ButtonView}>
                        <TouchableOpacity style={styles.cancel}>
                        <Text style={styles.buttonText}>Cancel</Text>
                        </TouchableOpacity>
                        <TouchableOpacity  style={styles.uploadPromo}>
                        <LinearGradient colors={['#41c7ae','#54e38e']} style={{height:'100%',width:'100%'}}>
                          <Text style={styles.buttonText}>Upload Geo</Text>
                        </LinearGradient>
                        </TouchableOpacity>
                    </View>
             </View>
          );
      }}
  }
  
  
  var styles = StyleSheet.create({
   
    geoIcon:{
        height:'30%',
        width:'100%',
        backgroundColor:'transparent',
        alignItems:'center'
    },
    fieldView:{
        height:'70%',
        width:'100%',
        backgroundColor:'transparent',
        alignItems:'center',
    },  
    datepicker:{
        marginTop:hp('1%'),
        width:'100%',
        height:'40%',
    },
    label:{
        fontFamily:'Montserrat-Light',
        fontSize: 11,
        alignSelf:'flex-start',
        marginTop:'5%',
        paddingLeft:'5%',
        color:'grey'
    },
    ButtonView:{
        width:'100%',
        height:'10%',
        backgroundColor:'transparent',
        marginTop:'12%',
        flexDirection:'row',
        justifyContent:'flex-start'
    },
    cancel:{
        height:'213%',
        width:'50%',
        backgroundColor:'#303147'
    },
    uploadPromo:{
        height:'213%',
        width:'50%',
        backgroundColor:'red'
    },  
    buttonText:{
      fontSize:16,
      fontFamily:'Montserrat-SemiBold',
      marginTop:'12%',
      color:'white',
      alignSelf:'center'
     
    },
  
    });

    export default withNavigation(Post_Geo);





  