import React, { Component } from 'react';
import { View,Dimensions ,Text,StyleSheet,TouchableWithoutFeedback,ImageBackground,Image,FlatList,TouchableOpacity} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { ScrollView } from 'react-native-gesture-handler';
import ElevatedView from 'react-native-elevated-view'
import { withNavigation } from 'react-navigation';
import Header from './Header';
import LinearGradient from 'react-native-linear-gradient';
import { Form, Item, Picker,Input,Label } from 'native-base';
import { ActivityIndicator } from 'react-native-paper';

var ImagePicker = require('react-native-image-picker');
import { DatePicker } from 'react-native-woodpicker';



var {height, width} = Dimensions.get('window');
 class Post_Promo extends Component{ 
    constructor(){
      super(); 
      this.state={
          layout:{
              height:height,
              width:width, 
            },
            baseURL:'http://api.neutralairpartner.com',
            Startdate: null ,
            Enddate: null,
            Title:'',   
            OriginSelected:'',
            DestinationSelected:'',
            filePath: {},
      } 
    }

    chooseFile = () => {
      var options = {
        title: 'Select Image',
        customButtons: [
          { name: 'customOptionKey', title: 'Choose Photo from Custom Option' },
        ],
        storageOptions: {
          skipBackup: true,
          path: 'images',
        },
      };
      ImagePicker.showImagePicker(options, response => {
        console.log('Response = ', response);
   
        if (response.didCancel) {
          console.log('User cancelled image picker');
        } else if (response.error) {
          console.log('ImagePicker Error: ', response.error);
        } else if (response.customButton) {
          console.log('User tapped custom button: ', response.customButton);
          alert(response.customButton);
        } else {
          let source = response;
          // You can also display the image using data:
          // let source = { uri: 'data:image/jpeg;base64,' + response.data };
          this.setState({
            filePath: source,
          });
        }
      });
    };

    handleDatePicker = data => {
        this.setState({ Startdate: data });
      };

      handleDatePicker2 = data => {
        this.setState({ Enddate: data });
      };

      handleTitle=(text)=>{
        this.setState({Title:(text)})
      } 

      handlePlaceholder = () =>
      this.state.Startdate
        ? this.state.Startdate.toDateString()
        : "No value Selected";

        handlePlaceholder2 = () =>
        this.state.Enddate
          ? this.state.Enddate.toDateString()
          : "No value Selected";

          onOriginValueChange(value) {
            this.setState({
              OriginSelected: value
            });
          }

          onDestinationValueChange(value) {
            this.setState({
              DestinationSelected: value
            });
          }


    render() {
      if (this.state.isloading) {
        return (
          <ActivityIndicator 
              animating={true} 
              color={'#303147'} 
              size={'large'}
              style={{ position:'absolute', left:0, right:0, bottom:0, top:0 }}/>
         );
       } else{
        return(
            <View style={{height:this.state.layout.height, width: this.state.layout.width, backgroundColor:'#f9f9f9',alignItems:"center"}}>
                <Header/>

                <ScrollView nestedScrollEnabled={true}
                  showsVerticalScrollIndicator={false}
                  style={{backgroundColor:'#f9f9f9'}}>
                  <ElevatedView
                    elevation={2} 
                    style={{ width:wp('92%'),backgroundColor:'white', height:this.state.layout.height*1.3, alignSelf:'flex-start',  borderRadius:3,alignSelf:'center',marginTop:'6%',alignItems:'center'}}>
                    
                    <View style={styles.geoIcon}>
                        <Image source={require('./Nap_Ios/post.png')} style={{height:'36%',width:'23%', marginTop:'6%'}}></Image>
                        <TouchableWithoutFeedback  onPress={this.chooseFile.bind(this)}>
                          <View style={styles.Picker}>
                            <Image source={require('./Nap_Ios/add2.png')} style={{height:'23%',width:'8%', marginTop:'9%'}}></Image>
                            <Text style={styles.uploadText}>Upload Banner Image</Text>
                          </View>
                        </TouchableWithoutFeedback>
                    </View>

                    <View style={styles.fieldView}>
                      {/* startdate */}
                      <Text style={styles.label}>Select Promo Start Date</Text>
                      <View style={{flexDirection:'row',backgroundColor:'transparent',width:'97%',height:'7%',justifyContent:'space-between'}}>
                          <DatePicker
                          style={styles.datepicker}
                          onDateChange={this.handleDatePicker}
                          value={this.state.Startdate}
                          title="Date Picker"
                          placeholderStyle={{color:'grey',fontSize:13,fontFamily:'Montserrat-Light',marginLeft:'10%'}}
                          placeholder={this.handlePlaceholder()}
                          iosPickerMode="date"
                          androidPickerMode="spinner"
                          />
                          <Image source={require('./Nap_Ios/calender.png')} style={{width:'5%',height:'42%',marginTop:'3%',}}/>
                      </View>
                      <View  style={{width:'98%',height:'0.3%' ,backgroundColor:'#D3D3D3'}}/>  

                      {/* Enddate */}
                      <Text style={styles.label}>Select Promo End Date</Text>
                      <View style={{flexDirection:'row',backgroundColor:'transparent',width:'97%',height:'7%',justifyContent:'space-between'}}>
                          <DatePicker
                          style={styles.datepicker}
                          onDateChange={this.handleDatePicker2}
                          value={this.state.Enddate}
                          title="Date Picker"
                          placeholderStyle={{color:'grey',fontSize:13,fontFamily:'Montserrat-Light',marginLeft:'10%'}}
                          placeholder={this.handlePlaceholder2()}
                          iosPickerMode="date"
                          androidPickerMode="spinner"
                          />
                          <Image source={require('./Nap_Ios/calender.png')} style={{width:'5%',height:'42%',marginTop:'3%',}}/>
                      </View>
                      <View  style={{width:'98%',height:'0.3%' ,backgroundColor:'#D3D3D3'}}/>  

                      {/* Origin */}
                    <Text style={styles.label}>Select Origin Airport</Text>
                    <View style={{flexDirection:'row',backgroundColor:'transparent',width:'98%',height:'7%',justifyContent:'space-around'}}>
                    <Picker
                        mode="dropdown"
                        style={{width:'100%',backgroundColor:'transparent'}}
                        textStyle={{color:'grey',fontFamily:'Montserrat-Light', fontSize:13}}
                        placeholder="Nothing Selected"
                        placeholderStyle={{ color: 'grey',fontSize:13,fontFamily:'Montserrat-Light'}}
                        selectedValue={this.state.OriginSelected}
                        onValueChange={this.onOriginValueChange.bind(this)}>
                        <Picker.Item label="Origin1" value="key0" />
                        <Picker.Item label="Origin2" value="key1" />
                        <Picker.Item label="Origin3" value="key2" />
                        <Picker.Item label="Origin4" value="key3" />
                        <Picker.Item label="Origin5" value="key4" />
                    </Picker>
                      <Image source={require('./Nap_Ios/pin.png')} style={{width:'5%',height:'50%',marginTop:'3%',}}/>
                    </View>
                     <View  style={{width:'98%',height:'0.3%' ,backgroundColor:'#D3D3D3'}}/> 


                     {/* destiantion */}
                     <Text style={styles.label}>Select Destination Airport</Text>
                      <View style={{flexDirection:'row',backgroundColor:'transparent',width:'98%',height:'7%',justifyContent:'space-around'}}>
                        <Picker
                            mode="dropdown"
                            style={{width:'100%',backgroundColor:'transparent'}}
                            textStyle={{color:'grey',fontFamily:'Montserrat-Light', fontSize:13}}
                            placeholder="Nothing Selected"
                            placeholderStyle={{ color: 'grey',fontSize:13,fontFamily:'Montserrat-Light'}}
                            selectedValue={this.state.DestinationSelected}
                            onValueChange={this.onOriginValueChange.bind(this)}>
                            <Picker.Item label="Origin1" value="key0" />
                            <Picker.Item label="Origin2" value="key1" />
                            <Picker.Item label="Origin3" value="key2" />
                            <Picker.Item label="Origin4" value="key3" />
                            <Picker.Item label="Origin5" value="key4" />
                        </Picker>
                      <Image source={require('./Nap_Ios/pin.png')} style={{width:'5%',height:'50%',marginTop:'3%',}}/>
                    </View>
                     <View  style={{width:'98%',height:'0.3%' ,backgroundColor:'#D3D3D3'}}/> 

                       {/* Airline */}
                     <Text style={styles.label}>Airline</Text>
                      <View style={{flexDirection:'row',backgroundColor:'transparent',width:'98%',height:'7%',marginTop:'1%',justifyContent:'space-around'}}>
                        <Picker
                            mode="dropdown"
                            style={{width:'100%',backgroundColor:'transparent'}}
                            textStyle={{color:'grey',fontFamily:'Montserrat-Light', fontSize:13}}
                            placeholder="Nothing Selected"
                            placeholderStyle={{ color: 'grey',fontSize:13,fontFamily:'Montserrat-Light'}}
                            selectedValue={this.state.DestinationSelected}
                            onValueChange={this.onOriginValueChange.bind(this)}>
                            <Picker.Item label="Origin1" value="key0" />
                            <Picker.Item label="Origin2" value="key1" />
                            <Picker.Item label="Origin3" value="key2" />
                            <Picker.Item label="Origin4" value="key3" />
                            <Picker.Item label="Origin5" value="key4" />
                        </Picker>
                      <Image source={require('./Nap_Ios/plane.png')} style={{width:'5%',height:'50%',marginTop:'3%',}}/>
                    </View>
                     <View  style={{width:'98%',height:'0.3%' ,backgroundColor:'#D3D3D3'}}/> 

                    {/* contact */}
                     <Text style={styles.label}>Contact</Text>
                      <View style={{flexDirection:'row',backgroundColor:'transparent',width:'98%',height:'7%',marginTop:'1%',justifyContent:'space-around'}}>
                        <Picker
                            mode="dropdown"
                            style={{width:'100%',backgroundColor:'transparent'}}
                            textStyle={{color:'grey',fontFamily:'Montserrat-Light', fontSize:13}}
                            placeholder="Nothing Selected"
                            placeholderStyle={{ color: 'grey',fontSize:13,fontFamily:'Montserrat-Light'}}
                            selectedValue={this.state.DestinationSelected}
                            onValueChange={this.onOriginValueChange.bind(this)}>
                            <Picker.Item label="Origin1" value="key0" />
                            <Picker.Item label="Origin2" value="key1" />
                            <Picker.Item label="Origin3" value="key2" />
                            <Picker.Item label="Origin4" value="key3" />
                            <Picker.Item label="Origin5" value="key4" />
                        </Picker>
                     
                    </View>
                     <View  style={{width:'98%',height:'0.3%' ,backgroundColor:'#D3D3D3'}}/> 

                     {/* Document or Image */}
                     <Text style={styles.DocLabel}>Document or Image</Text>
                     <TouchableWithoutFeedback  onPress={()=> alert('picker')}>
                          <View style={styles.DocPicker}>
                            <Image source={require('./Nap_Ios/add2.png')} style={{height:'23%',width:'6%', marginTop:'9%'}}></Image>
                          </View>
                    </TouchableWithoutFeedback>
                    </View>
                  </ElevatedView> 
                </ScrollView>

                    <View style={styles.ButtonView}>
                    <TouchableOpacity style={styles.cancel}>
                        <Text style={styles.buttonText}>Cancel</Text>
                        </TouchableOpacity>
                        <TouchableOpacity  style={styles.uploadPromo}>
                        <LinearGradient colors={['#41c7ae','#54e38e']} style={{height:'100%',width:'100%'}}>
                          <Text style={styles.buttonText}>Upload Geo</Text>
                        </LinearGradient>
                      </TouchableOpacity>
                    </View>
             </View>
          );
      }}
  }
  
  
  var styles = StyleSheet.create({
   
    geoIcon:{
        height:'26%',
        width:'100%',
        backgroundColor:'transparent',
        alignItems:'center'
    },
    fieldView:{
        height:'74%',
        width:'100%',
        marginTop:'2%',
        backgroundColor:'transparent',
        alignItems:'center',
    },  
    datepicker:{
        marginTop:hp('1%'),
        width:'100%',
        height:'40%',
    },
    label:{
        fontFamily:'Montserrat-Light',
        fontSize: 12,
        alignSelf:'flex-start',
        marginTop:'5%',
        paddingLeft:'1%',
        color:'grey'
    },
    ButtonView:{
        width:'100%',
        height:'10%',
        backgroundColor:'transparent',
        marginTop:'12%',
        flexDirection:'row',
        justifyContent:'flex-start'
    },
    cancel:{
        height:'213%',
        width:'50%',
        backgroundColor:'#303147'
    },
    uploadPromo:{
        height:'213%',
        width:'50%',
        backgroundColor:'red'
    },
    buttonText:{
      fontSize:15,
      fontFamily:'Montserrat-SemiBold',
      marginTop:'12%',
      color:'white',
      alignSelf:'center'
     },
     Picker:{
       height:'42%',
       width:'92%',
       alignSelf:'center',
       backgroundColor:'#D3D3D3',
       marginTop:'3%',
       alignItems:'center'
},
DocPicker:{
  height:'15%',
  width:'97%',
  alignSelf:'center',
  backgroundColor:'#D3D3D3',
  marginTop:'3%',
  alignItems:'center'
},
uploadText:{
  fontFamily:'Montserrat-Regular',
  fontSize:11,
  alignSelf:'center',
  color:'grey',
  marginTop:'3%'
},
DocLabel:{
      fontFamily:'Montserrat-Light',
        fontSize: 12,
        alignSelf:'flex-start',
        marginTop:'10%',
        paddingLeft:'1%',
        color:'grey'
}
  
    });

    export default withNavigation(Post_Promo);





  