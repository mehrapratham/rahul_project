import React, { Component } from 'react';
import { View,Dimensions ,Text,StyleSheet,TouchableOpacity,Image,AsyncStorage,ImageBackground} from 'react-native';
import { FluidNavigator, Transition } from 'react-navigation-fluid-transitions';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Header from './Header';
import { ScrollView } from 'react-native-gesture-handler';
import { Accordion, Tabs,Tab,Container, Content } from "native-base";
import ElevatedView from 'react-native-elevated-view'
import ProfileAbout from './ProfileAbout.js';
import ProfileFees from './ProfileFees&Rates.js';
import ProfilePersonel from './ProfilePersonel.js';
import ProfileActivity from './ProfileActivity.js';
import { ActivityIndicator } from 'react-native-paper';
import { withNavigation,createDrawerNavigator, DrawerItems,createAppContainer } from 'react-navigation';
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';
import Sidebar from './Sidebar'
import Carousel from 'react-native-carousel-view';
import { Card,Avatar,Tooltip } from 'react-native-elements';
import {OptimizedFlatList} from 'react-native-optimized-flatlist'
import AppContain from './router'


var {height, width} = Dimensions.get('window');

 class Profile extends Component{
    constructor(){
      super();
      global.info=''
      global.description=''
      global.mawbaddress=''
      global.destinationnotes=''
      global.bankname=''
      global.accountname=''
      global.accountnumber=''
      global.fax=''
      global.swift=''
      global.telephone=''
      global.bankaddress=''
      global.feesUrls=[],
      global.CompanyPromo=''
      global.companyName=''
      global.country=''
      global.importhandover=''
      global.fob=''
      global.dap=''
      global.promo=''
      global.rates=''
      global.companyPersonel=''
      this.state={
          layout:{
              height:height,
              width:width,
            },
            baseURL:'http://api.neutralairpartner.com',
            api:'/v1/company/',
            Id:'/',
            CompanyData:[],
            cityAPI:'http://api.neutralairpartner.com/v1/cities/',
            logo: require('./Nap_Ios/a2.png'),
            name:'', 
            datas:'', 
            bankinfo:'',
            cityName:'',
            isloading:true,
            country:'',
            companyemail:'',companyphone:'',companyweb:'',companyfax:'',address1:'',address2:'',
            mawbaddress:'',  accountname:'',accountnumber,fax:'',swift:'',telephone:'',bankaddress:'',
            countryname:'',logowebpath:'', importhandoverUrls:'', fobUrls:'', dapUrls:'',acbCargoCharters:'',
            specialisations:'', array:['one','two','three'], branch:'',specialisation:'', trade:'',
            height: 'auto',
            aboutHeight: 0,
            personelHeight: 0,
            ratesHeight: 0,
            activityHeight: 0

      }
      global.height= null
      
    }




    componentDidMount(){
      this.onFetchCompanyDetails(global.token, global.id)
    }

    getHeight(key,val){
      console.log(val,"newVal")
      // this.setState({aboutHeight: val})
      if(key === 'about'){
        console.log(val,"about")
        this.setState({aboutHeight: val})
      }
      if(key === 'personel'){
        console.log(val,"personel")
        this.setState({personelHeight: val})
      }
      if(key === 'rates'){
        this.setState({ratesHeight: val})
      }
      if(key === 'activity'){
        this.setState({activityHeight: val})
      }
    }
  

    componentWillUnmount (){
      params.callHome();
 }

 all(){
  this.onFetchCompanyDetails(global.token, global.id)
 }
 
    //api call//
    async onFetchCompanyDetails(token, id) {
      try {
       let response = await fetch(
        this.state.baseURL+this.state.api+id+'/profile',
        {
          'method': 'GET',
          headers: {
            'Accept': 'application/json',
           'Content-Type': 'application/json',
           'Authorization':'Bearer'+' '+token
           },
       }
      );
       if (response.status >= 200 && response.status < 300) {
         
         response.json().then(data => {

////////////////////logo///////////////////////////
          if(data.logo.length > 0){
            var url={uri: data.logo[0].path}
            this.setState({logo:url})
          } else{
            var url=require('./Nap_Ios/a2.png')
            this.setState({logo:url})
          }

////////////////companyPersonel////////////////////
            global.companyPersonel= data.userSettings
           
/////////////////////Adresses//////////////////////
            this.setState({companyweb:data.companyInfo[0].companyWebsite})
            this.setState({companyemail:data.companyInfo[0].companyEmail})
            this.setState({companyphone:data.companyInfo[0].companyTel})
            this.setState({companyfax:data.companyInfo[0].companyFax})
            this.setState({address1:data.companyInfo[0].address1})
            this.setState({address2:data.companyInfo[0].address2})


//////////////////////////About Data////////////////////////////
            global.description=data.companyInfo[0].description
            global.mawbaddress=data.companyData[0].mawbAddress
            global.destinationnotes=data.companyData[0].destinationNotes

           
            ////////////bank Info///////////////////
            if(data.companyBankInfo[0].referenceBankName==null){
              global.bankname= 'N/A'
            }else{
              global.bankname=data.companyBankInfo[0].referenceBankName
            }
        
          
            if(data.companyBankInfo[0].referenceBankAccount==null){
              global.accountname= 'N/A'
            }else{
              global.accountname=data.companyBankInfo[0].referenceBankAccount
            }
           
           
            if(data.companyBankInfo[0].referenceBankAccnumber==null){
              global.accountnumber= 'N/A'
            }else{
              global.accountnumber=data.companyBankInfo[0].referenceBankAccnumber
            }
           
            
            if(data.companyBankInfo[0].referenceBankSwift==null){
              global.swift= 'N/A'
            }else{
              global.swift=data.companyBankInfo[0].referenceBankSwift
            }
            
            
            if(data.companyBankInfo[0].referenceBankFax==null){
              global.fax= 'N/A'
            }else{
              global.fax=data.companyBankInfo[0].referenceBankFax
            }
            
            
            if(data.companyBankInfo[0].referenceBankAddress==null){
              global.bankaddress= 'N/A'
            }else{
              global.bankaddress=data.companyBankInfo[0].referenceBankAddress
            }
           
            if(data.companyBankInfo[0].bankTel==null){
              global.telephone= 'N/A'
            }else{
              global.telephone=data.companyBankInfo[0].bankTel
            }
//data.branch.branchCity
//data.specializations
//data.airIndustryExpertise
           this.setState({branch: data.branch})
           this.setState({specialisation: data.specializations})
           this.setState({trade: data.airIndustryExpertise})


////////////////Name And Country Data/////////////////
            this.state.CompanyData.push(data)
            this.setState({name:(data.companyName)})
            this.setState({countryname: data.countries.countryname})
            global.country= data.countries.countryname
            global.companyName=data.companyName
            var code=this.state.CompanyData[0].city
            this.onFetchCity(global.token,code)
            

////////////////Rates and Fees/////////////////////

            global.importhandover= data.importhandover
            global.fob= data.fob
            global.dap= data.dap
            global.rates= data.rates

            global.promo= data.myPromos

            if(data.airIndustryExpertise!= null){
              this.setState({specialisations: data.airIndustryExpertise})
            } else{
              this.setState({specialisations: 'N/A'})
            }
          
          });
          
         
          
        } else{
          alert('JWT token not found')
        }
     } catch (errors) {
        alert(errors);
       } 
  }
  //api call//

  //City Api//
  async onFetchCity(token,code){
    try {
      let response = await fetch(
       this.state.cityAPI+code,
       {
         'method': 'GET',
         headers: {
           'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization':'Bearer'+' '+token
          },
      }
     ); 
      if (response.status >= 200 && response.status < 300) {
        response.json().then(data => {
            
          this.setState({cityName: data.cityName})
            
         }); 
         this.setState({isloading:false})
       } else{
         alert('JWT token not found')
       }
    } catch (errors) {
       alert(errors);
      } 
      }
  //City Api//


 
 
    render() {
      let {aboutHeight,personelHeight,activityHeight,ratesHeight} = this.state
      const dataArray = [
        { title: "Charter Broker", content: "Cargo Charter company operates per year"+"  "+this.state.acbCargoCharters+"  "+ "Comodities or Tradelanes Company is specialized at" },
        { title: "Express Operator", content: "ECW Destination Airport"+"     "+"    "+"Top Vendors" },
      ];
      if (this.state.isloading) {
        return (
          <OrientationLoadingOverlay
          visible={true}
          color="#303147"
          indicatorSize="large"
          messageFontSize={14}
          style={{
            position:'absolute', left:0, right:0, bottom:0, top:0 }}
          >
        </OrientationLoadingOverlay>
         );
       } else{
        return(
          <Transition appear='horizontal' >
            <View style={{height:this.state.layout.height*1, width: this.state.layout.width, backgroundColor:'transparent'}}>
              <Header/>
              <ScrollView style={styles.dataView}
                nestedScrollEnabled={true}>
                    <View style={styles.container}>
                        <View style={styles.profiledown}></View>
                        <View style={styles.profilemiddle}></View>
                        <View style={styles.profiletop}>
                       
                        
                        <Image
                              style={{width:85, height:85,backgroundColor:'white',borderRadius:85/2,borderWidth:hp('0.7%'),borderColor:'white'}}
                              resizeMode={'contain'} 
                              source={this.state.logo}></Image>
                            
                       
                        <Text style={styles.title}>{this.state.name}</Text>
                        <Text style={styles.country}>{this.state.cityName} , {this.state.countryname}</Text>
                        
                        <View  style={{width:wp('32%'),height:hp('6.5%'), marginBottom:hp('18%'),backgroundColor:'#2B303A', alignItems:'center',borderRadius:45}}>
                        <TouchableOpacity  onPress={()=> this.props.navigation.navigate('ChatScreen')} style={{height:'100%', width:'100%',alignItems:'center'}}>
                        <Text style={{fontSize:hp('2.3%'),fontFamily:'Montserrat-Regular',marginTop:hp('1.7%'),color:'white'}}>Send Message</Text>
                        </TouchableOpacity>
                        </View>
                      
                        </View>
                        
                    </View>

                    <View style={{ width: this.state.layout.width, backgroundColor:'#f9f9f9',height:'auto'}}>
                        <ElevatedView 
                            elevation={4}
                            style={{ width:wp('100%'),backgroundColor:'white', height:hp('40%'), alignItems:'center',  borderBottomLeftRadius:4, borderBottomRightRadius:4,justifyContent:'flex-start'}}>
                        
                        {/* slider */}
                        <View style={{width:'88%',height:'auto', backgroundColor:'transparent',marginTop:hp('3%')}}>
                        <Carousel
                        scrollEnabled={false}
                        width={290}
                        height={100}
                        delay={1000}
                        hideIndicators={true}
                        indicatorAtBottom={true}
                        indicatorSize={20}
                        indicatorText="."
                        inactiveIndicatorText='o'
                        inactiveIndicatorColor='#332863'
                        indicatorColor="#303147"
                        //onPageChange={this.corousel()}
                        contentContainerStyle={{justifyContent:'flex-start',flex:1}}>

                          <View style={styles.contentContainer}>
                            <Image source={require('./Nap_Ios/trade.png')} style={{height:37, width:35, marginTop:hp('1%')}}></Image>
                            <View style={{flexDirection:'column',justifyContent:'flex-start'}}>
                            <Text style={{marginTop:'2%',fontFamily:'Montserrat-Regular',fontSize:14,color:'grey',marginLeft:hp('2.5%')}}>Trade</Text>
                            <OptimizedFlatList
                               data={this.state.trade}
                               scrollEnabled={false}
                               numColumns={1}
                               style={{marginLeft:hp('2.5%')}}
                              renderItem={({ item: data }) => {
                                
                                return (
                                <Text style={{fontFamily:'Montserrat-Regular',fontSize:13}}>{data.industryExpertise}</Text>
                            
                                )}}
                                listKey={(item, index) => 'D' + index.toString()}
                                keyExtractor={(item, index) => index.toString()} /> 
                              </View>
                        </View>
                        
                        <View style={styles.contentContainer}>
                            <Image source={require('./Nap_Ios/branches.png')} style={{height:40, width:40, marginTop:hp('1%')}}></Image>
                            <View style={{flexDirection:'column',justifyContent:'flex-start'}}>
                            <Text style={{marginTop:'2%',fontFamily:'Montserrat-Regular',fontSize:14,color:'grey',marginLeft:hp('2.5%')}}>special</Text>
                            <OptimizedFlatList
                               data={this.state.specialisation}
                               scrollEnabled={false}
                               numColumns={2}
                               style={{marginLeft:hp('2.5%')}}
                              renderItem={({ item: data }) => {
                                
                                return (
                                <Text style={{fontFamily:'Montserrat-Regular',fontSize:13}}>{data.specialization}</Text>
                            
                                )}}
                                listKey={(item, index) => 'D' + index.toString()}
                                keyExtractor={(item, index) => index.toString()} /> 
                              </View>
                        </View>
                        
                        <View style={styles.contentContainer}>
                            <Image source={require('./Nap_Ios/special.png')} style={{height:40, width:40, marginTop:hp('1%')}}></Image>
                            <View style={{flexDirection:'column',justifyContent:'flex-start'}}>
                            <Text style={{marginTop:'2%',fontFamily:'Montserrat-Regular',fontSize:14,color:'grey',marginLeft:hp('2.5%')}}>Branch</Text>
                            <OptimizedFlatList
                               data={this.state.branch}
                               scrollEnabled={false}
                               numColumns={2}
                               style={{marginLeft:hp('2.5%')}}
                              renderItem={({ item: data }) => {
                                
                                return (
                                <Text style={{fontFamily:'Montserrat-Regular',fontSize:13}}>{data.branchCity}</Text>
                            
                                )}}
                                listKey={(item, index) => 'D' + index.toString()}
                                keyExtractor={(item, index) => index.toString()} /> 
                              </View>
                        </View>
                        </Carousel>
                        </View>
                        {/* slider */}

                        {/* curveData */}
                        <View style={{ backgroundColor:'transparent'}}>
                        <ImageBackground source={require('./Nap_Ios/line.png')} style={{ width:230, height:50, marginTop:hp('5%'),flexDirection:'row',justifyContent:'space-between'}}>
                        <View style={{width:'10%',height:'27%',backgroundColor:'white',marginTop:'1%'}}>
                                  <Tooltip popover={
                                  <View>
                                  <Text style={styles.curveDataHead}>EXPORTS</Text>
                                  <Text style={styles.curveDataInfo}>Percent:10</Text>
                                  </View>}
                                    withOverlay={false}
                                    backgroundColor={'#D3D3D3'}
                                    width={100}
                                    height={50}>
                                       <View style={{width:'100%',height:'100%',borderRadius:70,backgroundColor:'#3399ff',alignItems:'center',borderColor:'#0099ff'}}>
                                      <View style={{width:'75%',height:'72%',backgroundColor:'white',marginTop:'15%',borderRadius:70}}></View>
                                      </View>
                                  </Tooltip>
                                  </View>

                                  <View style={{width:'10%',height:'15%',backgroundColor:'white',marginTop:'14%'}}>
                                  <Tooltip popover={
                                  <View>
                                  <Text style={styles.curveDataHead}>IMPORTS</Text>
                                  <Text style={styles.curveDataInfo}>Percent:10</Text>
                                  </View>
                                  }
                                    withOverlay={false}
                                    backgroundColor={'#D3D3D3'}
                                    width={100}
                                    height={50}>
                                      <View style={{width:'100%',height:'85%',borderRadius:70,backgroundColor:'#0099ff',alignItems:'center',borderColor:'#0099ff'}}>
                                      <View style={{width:'75%',height:'72%',backgroundColor:'white',marginTop:'15%',borderRadius:70}}></View>
                                      </View>
                                  </Tooltip>
                                  </View>

                                  <View style={{width:'10%',height:'27%',backgroundColor:'white'}}>
                                  <Tooltip popover={
                                    <View>
                                    <Text style={styles.curveDataHead}>CROSS-TRADE</Text>
                                    <Text style={styles.curveDataInfo}>Percent:10</Text>
                                    </View>}
                                    withOverlay={false}
                                    backgroundColor={'#D3D3D3'}
                                    width={120}
                                    height={50}>
                                      <View style={{width:'100%',height:'100%',borderRadius:72,backgroundColor:'#0099ff',alignItems:'center',borderColor:'#0099ff'}}>
                                      <View style={{width:'73%',height:'75%',backgroundColor:'white',marginTop:'12%',borderRadius:70}}></View>
                                      </View>
                                  </Tooltip>
                                  </View>

                                  <View style={{width:'10%',height:'12%',backgroundColor:'white',marginTop:'16%'}}>
                                  <Tooltip popover={
                                      <View>
                                      <Text style={styles.curveDataHead}>E-COMMERCE</Text>
                                      <Text style={styles.curveDataInfo}>Percent:10</Text>
                                      </View>
                                          }
                                    withOverlay={false}
                                    backgroundColor={'#D3D3D3'}
                                    width={120}
                                    height={50}>
                                      <View style={{width:'100%',height:'85%',borderRadius:70,backgroundColor:'#0099ff',alignItems:'center',borderColor:'#0099ff'}}>
                                      <View style={{width:'73%',height:'75%',backgroundColor:'white',marginTop:'12%',borderRadius:70}}></View>
                                      </View>
                                  </Tooltip>
                                  </View>
                        </ImageBackground>
                        </View>
                        {/* curveData */}
                        </ElevatedView>

                        <ElevatedView 
                            elevation={4}
                            style={{ width:wp('100%'),backgroundColor:'white', height:hp('40%'), alignSelf:'center', borderRadius:10  ,marginTop:hp('2%')}}>
                            <Text style={styles.headerText}>Specializations</Text>
                            <View style={styles.line}></View>

                            <Accordion dataArray={dataArray} expanded={0} icon="add" expandedIcon="remove"

                                 animation={true}
                                 headerStyle={{backgroundColor:'white',fontFamily:'Montserrat-UltraLight'}}
                                 contentStyle={{color:'grey',paddingLeft:'5%',fontFamily:'Montserrat-Light',fontSize:14,flex:1}}
                                 />
                            
                            
                        </ElevatedView>

                        {/* <ElevatedView 
                            elevation={4}
                            style={{  width:wp('95%'),backgroundColor:'transparent', flex:1, alignSelf:'center',  borderRadius:10 ,marginTop:hp('2%')}}> */}
                                    
                                    
                                    <Tabs initialPage={0}
                                    style={{  width:wp('95%'), alignSelf:'center',  borderRadius:10 ,marginTop:hp('5%'),}}
                                        tabBarUnderlineStyle={styles.tabBarUnderlineStyle}
                                        onChangeTab={()=>this.all()}>
                                        
                                        <Tab heading="About"
                                        activeTabStyle={styles.activeTabStyle}
                                        activeTextStyle={styles.activeTextStyle}
                                        textStyle={styles.textStyle}
                                        >
                                        {/* <Content contentContainerStyle={{flex: 1}}> */}
                                        {/* <View style={aboutHeight && {height: aboutHeight}}> 
                                          <ProfileAbout getHeight={this.getHeight.bind(this, 'about')}/>
                                        </View> */}
                                          <Text>asdsddadsdsd</Text>
                                          <Text>asdsddadsdsd</Text>
                                          <Text>asdsddadsdsd</Text>
                                          <Text>asdsddadsdsd</Text>
                                          <Text>asdsddadsdsd</Text>
                                          <Text>asdsddadsdsd</Text>
                                          <Text>asdsddadsdsd</Text>
                                        {/* </Content> */}
                                        </Tab>
                                        
                                        
                                        <Tab heading="Personel" 
                                        activeTabStyle={styles.activeTabStyle}
                                        activeTextStyle={styles.personelactiveTextStyle}
                                        textStyle={styles.personelText}>
                                        {/* <Content contentContainerStyle={{flex: 1}}> */}
                                        {/* <View style={personelHeight && {height: personelHeight}}>
                                          <ProfilePersonel getHeight={this.getHeight.bind(this, 'personel')}/>
                                        </View> */}
                                        <Text>asdsddadsdsd</Text>
                                          <Text>asdsddadsdsd</Text>
                                          <Text>asdsddadsdsd</Text>
                                          <Text>asdsddadsdsd</Text>
                                          <Text>asdsddadsdsd</Text>
                                          <Text>asdsddadsdsd</Text>
                                          <Text>asdsddadsdsd</Text>
                                          <Text>asdsddadsdsd</Text>
                                          <Text>asdsddadsdsd</Text>
                                          <Text>asdsddadsdsd</Text>
                                          <Text>asdsddadsdsd</Text>
                                          <Text>asdsddadsdsd</Text>
                                          <Text>asdsddadsdsd</Text>
                                          <Text>asdsddadsdsd</Text>
                                          <Text>asdsddadsdsd</Text>
                                          <Text>asdsddadsdsd</Text>
                                          <Text>asdsddadsdsd</Text>
                                          <Text>asdsddadsdsd</Text>
                                          <Text>asdsddadsdsd</Text>
                                          <Text>asdsddadsdsd</Text>
                                          <Text>asdsddadsdsd</Text>
                                        {/* </Content> */}
                                        </Tab>
                                      

                                       
                                        <Tab heading="Rates"
                                        activeTabStyle={styles.activeTabStyle}
                                        activeTextStyle={styles.activeTextStyle}
                                        textStyle={styles.textStyle}>
                                        {/* <Content contentContainerStyle={{flex: 1}}> */}
                                          {/* <View style={ratesHeight && {height: ratesHeight}}>
                                            <ProfileFees getHeight={this.getHeight.bind(this, 'rates')}/>
                                          </View> */}
                                          <Text>asdsddadsdsd</Text>
                                          <Text>asdsddadsdsd</Text>
                                          <Text>asdsddadsdsd</Text>
                                          
                                        {/* </Content> */}
                                        
                                        </Tab>


                                        <Tab heading="Activity"
                                        activeTabStyle={styles.activeTabStyle}
                                        activeTextStyle={styles.activeTextStyle}
                                        textStyle={styles.textStyle}>
                                        {/* <Content contentContainerStyle={{flex: 1}}> */}
                                        {/* <View style={activityHeight && {height: activityHeight}}>
                                          <ProfileActivity  getHeight={this.getHeight.bind(this, 'activity')}/>
                                        </View> */}
                                        <Text>asdsddadsdsd</Text>
                                          <Text>asdsddadsdsd</Text>
                                          <Text>asdsddadsdsd</Text>
                                          <Text>asdsddadsdsd</Text>
                                          <Text>asdsddadsdsd</Text>
                                          <Text>asdsddadsdsd</Text>
                                          <Text>asdsddadsdsd</Text>
                                          <Text>asdsddadsdsd</Text>
                                          <Text>asdsddadsdsd</Text>
                                          <Text>asdsddadsdsd</Text>
                                          <Text>asdsddadsdsd</Text>
                                       <Text>asdsddadsdsd</Text>
                                          <Text>asdsddadsdsd</Text>
                                          <Text>asdsddadsdsd</Text>
                                          <Text>asdsddadsdsd</Text>
                                          <Text>asdsddadsdsd</Text>
                                          <Text>asdsddadsdsd</Text>
                                          <Text>asdsddadsdsd</Text>
                                          <Text>asdsddadsdsd</Text>
                                          <Text>asdsddadsdsd</Text>
                                             <Text>asdsddadsdsd</Text>
                                          <Text>asdsddadsdsd</Text>
                                          <Text>asdsddadsdsd</Text>
                                          <Text>asdsddadsdsd</Text>
                                          <Text>asdsddadsdsd</Text>
                                          <Text>asdsddadsdsd</Text>
                                          <Text>asdsddadsdsd</Text>
                                          <Text>asdsddadsdsd</Text>
                                        {/* </Content> */}
                                        </Tab>
                                    </Tabs>
                                {/* <ElevatedView 
                                  style={{width:wp('95%'), alignSelf:'center',  borderRadius:10 ,marginTop:hp('2%'),height:'auto'}}>
                                  <AppContain/>
                                </ElevatedView> */}
                                  
                              {/* </ElevatedView> */}

                              <ElevatedView 
                              elevation={4}
                              style={{ width:wp('100%'),backgroundColor:'white',  alignSelf:'center',  borderRadius:10 }}>
                               <Text style={styles.headerText}>Contact & Locations</Text>
                              <View style={styles.line}></View>
                              
                                  <View style={styles.cell}>
                                  <View style={styles.head}>
                                      <Text style={styles.headText}>HQ Contacts</Text>
                                  </View>

                                  <View style={styles.body}>
                                      <View style={{flexDirection:'row',height:'auto',width:'auto',marginTop:'1%'}}>
                                      <Text style={styles.bodyTexthead}>T. </Text>
                                      <Text style={styles.bodyText}>{this.state.companyphone}</Text>
                                      </View>

                                      <View style={{flexDirection:'row',height:'auto',width:'auto',marginTop:'1%'}}>
                                      <Text style={styles.bodyTexthead}>F. </Text>
                                      <Text style={styles.bodyText}>{this.state.companyfax}</Text>
                                      </View>

                                      <View style={{flexDirection:'row',height:'auto',width:'auto',marginTop:'1%'}}>
                                      <Text style={styles.bodyTexthead}>E. </Text>
                                      <Text style={styles.bodyText}>{this.state.companyemail}</Text>
                                     
                                      </View>

                                      <View style={{flexDirection:'row',height:'auto',width:'auto',marginTop:'1%'}}>
                                      <Text style={styles.bodyTexthead}>W. </Text>
                                      <Text style={styles.bodyText}>{this.state.companyweb}</Text>
                                      </View>
                                  </View>
                                  </View>

                                  <View style={styles.cell}>
                                  <View style={styles.head}>
                                      <Text style={styles.headText}>ADDRESS</Text>
                                  </View>

                                  <View style={styles.body}>
                                      <Text style={styles.bodyText}>{this.state.address1}</Text>
                                      <Text style={styles.bodyText}>{this.state.address2}</Text>
                                      
                                  </View>
                                  </View>

                                  <View style={styles.cell}>
                                  <View style={styles.head}>
                                      <Text style={styles.headText}>NAP CO-ordinator</Text>
                                  </View>

                                  <View style={styles.body}>
                                      <Text style={styles.bodyText}>Vijay Nadar</Text>

                                      <View style={{flexDirection:'row',height:'auto',width:'auto',marginTop:'1%'}}>
                                      <Text style={styles.bodyTexthead}>E. </Text>
                                      <Text style={styles.bodyText}>NAP@in.mrspedag.com</Text>
                                      </View>

                                      <View style={{flexDirection:'row',height:'auto',width:'auto',marginTop:'1%'}}>
                                      <Text style={styles.bodyTexthead}>T. </Text>
                                      <Text style={styles.bodyText}>123456789</Text>
                                      </View>

                                  </View>
                                  

                                      <View style={{ width:wp('85%'),height:hp('20%'), backgroundColor:'transparent',flexDirection:'row',alignItems:'center',justifyContent:'space-around'}}>
                                          <TouchableOpacity style={{width:'45%',height:'45%',backgroundColor:'transparent',}}>
                                            <View style={{width:'100%', height:'100%',backgroundColor:'#2B303A',borderRadius:40,alignItems:'center'}}>
                                                  <Text style={{color:'white',fontSize:15,marginTop:'13%',fontFamily:'Montserrat-Regular'}}>Branch Office 1</Text>
                                            </View>
                                          </TouchableOpacity>

                                          <TouchableOpacity style={{width:'45%',height:'45%',backgroundColor:'transparent',}}>
                                            <View style={{width:'100%', height:'100%',backgroundColor:'#2B303A',borderRadius:40,alignItems:'center'}}>
                                                  <Text style={{color:'white',fontSize:15,marginTop:'13%',fontFamily:'Montserrat-Regular'}}>Branch Office 2</Text>
                                            </View>
                                          </TouchableOpacity>
                                      </View>

                                  </View>
                      </ElevatedView>
                    </View>

                    {/* <View style={{width: this.state.layout.width, height: hp('17%'),backgroundColor:'white'}}>
                      <TouchableOpacity style={{width:wp('45%'),height:hp('4.5%'),alignSelf:'center',backgroundColor:'transparent',}}>
                        <View  style={{width:'100%',height:'200%',borderRadius:45,alignItems:'center',backgroundColor:'#C1DBE3'}}>
                        <Text style={{fontSize:18,fontFamily:'Gill Sans',marginTop:hp('2.5%'),color:'black'}}>Support Center</Text>
                        </View>
                       </TouchableOpacity>
                       <View style={{width: this.state.layout.width/1.4, height:hp('6%'),flexDirection:'row',justifyContent:'space-around',backgroundColor:'white',alignSelf:'center',marginTop:hp('5%')}}>
                            <Text style={{color:'grey',fontFamily:'Gill Sans',fontSize:16,paddingLeft:'7%'}}>Privacy</Text>
                            <Text style={{color:'grey',fontFamily:'Gill Sans',fontSize:16}}>Terms</Text>
                            <Text style={{color:'grey',fontFamily:'Gill Sans',fontSize:16}}>Credit Terms</Text>
                        </View>
                      </View> */}

              </ScrollView>
             </View>
             </Transition>
          
          );
      }
    }
  }
  
  const AppDrawerNavigator= createDrawerNavigator({
  
    //Home: Home,
    // Promo: Promo,
     //Alert: Alert,
    // Geolocator: Geolocator,
    Profile: Profile
  },{
    contentComponent: props => <Sidebar />,
    drawerWidth: width/4,
    drawerBackgroundColor:'#303147',
    contentOptions:{
      activeTintColor:'red'
    }
  })
const AppContainer= createAppContainer(AppDrawerNavigator)
  
  var styles = StyleSheet.create({
    container: {
      flex: 1,
      height:hp('38%'),
      width:wp('100%'),
      justifyContent: 'flex-start',
      alignItems:'center',
      backgroundColor:'#303147'
    },
  
    dataView:{
        height:'auto',
        width:wp('100%'),
        alignSelf:'center',
        backgroundColor:'#D3D3D3',
    },
    profiledown:{
        height:hp('3.5%'),
        width:wp('70%'),
        backgroundColor:'grey',
        marginTop:hp('10%'),
        borderTopLeftRadius: 8, 
        borderTopRightRadius: 8,
        
    }, 
    profilemiddle:{
        height:hp('3.5%'),
        width:wp('80%'),
        backgroundColor:'#D3D3D3',
        borderTopLeftRadius: 8, 
        borderTopRightRadius: 8,
        
    }, 
    profiletop:{
        height:hp('25%'),
        width:wp('90%'),
        backgroundColor:'white',
        alignItems:'center',
        justifyContent:'center',
        borderTopLeftRadius: 8, 
        borderTopRightRadius: 8,
    },
    
    dataContainer:{
        height:hp('38%'),
        width:wp('100%'),
        justifyContent: 'flex-start',
        alignItems:'center',
        backgroundColor:'#f9f9f9'
    },
    
    line:{
        width:'100%',
        height:'0.3%',
        backgroundColor:'#D3D3D3',
        marginTop:'7%'
    },
    headerText:{
        fontSize:17,
        paddingTop:'6%',
        paddingLeft:'5%',
       
        fontFamily:'Montserrat-Regular',
    },
    tabStyle : {
        backgroundColor: 'red',
        justifyContent: 'center',
        width: 200,
        height: 40,
      },
      activeTabStyle: {
        backgroundColor: 'transparent',
        height: 40,
      },
      textStyle: {
         color: 'black',
         fontFamily:'Montserrat-Light',
         fontSize:13,
      },
      personelText:{
        color: 'black',
        fontFamily:'Montserrat-Light',
        fontSize:13,
      },
      personelactiveTextStyle: {
       
        color: '#332863',
        fontFamily:'Montserrat-Light',
        fontSize:13,
      },
      activeTextStyle: {
        // color: 'white',
        color: '#332863',
        fontFamily:'Montserrat-Light',
        fontSize:13,
      },
      tabBarUnderlineStyle: {
        backgroundColor: '#332863',
        height: 1
      },

      cell:{
        height:'auto',
        width:wp('85%'),
        marginLeft:wp('2.5%'),
        backgroundColor:'transparent',
         marginTop:hp('2%')
    },
    head:{
      width:'100%',
      height:'auto',
      backgroundColor:'transparent',
      alignItems:'center',
      flexDirection:'row'
   },

   headText:{
       fontSize:16,
       paddingLeft:'1%',
       color:'#332863',
       
       fontFamily:'Montserrat-Bold'
   },
   body:{
       width:'100%',
       height:'auto',
       backgroundColor:'transparent',
       marginTop:5,
       alignItems:'flex-start',
       //justifyContent:'flex-start'
    },

    bodyTexthead:{
       fontSize:16,
       paddingLeft:'1%',
       color:'grey',
      
       fontFamily:'Montserrat-Regular',
   },
   bodyText:{
    fontSize:14,
    paddingTop:'1%',
    paddingLeft:'1%',
    color:'grey',
    fontFamily:'Montserrat-Light',
},
profilepicture:{
  height:'61%',
  width:'30%',
  borderWidth:6,
  borderColor:'white',
  backgroundColor:'transparent',
   borderRadius:100,
  
  alignItems:'center'
},
title:{
  fontSize:hp('2.5%'),
    paddingLeft:'1%',
    color:'#332863',
    width:'95%',
    height:'auto',
    fontFamily:'Montserrat-Bold',
    textAlign: "center",
  },
country:{
  fontSize:hp('2.3%'),
    paddingLeft:'1%',
    color:'grey',
    fontFamily:'Montserrat-Regular',
  marginBottom:'3%'
},
trade:{
    height:'40%',
    width:'90%',
    backgroundColor:'transparent',
    marginBottom:'25%',
    flexDirection:'row'
},
tradeImage:{
  height:'60%',
  width:'27%',
  alignItems:'center',
  marginTop:'2%',
  marginLeft:'1%'
},
tradeText:{
  height:'60%',
  width:'80%',
  backgroundColor:'transparent',
},
tradeinner:{
  fontSize:14,
    color:'grey',
    fontFamily:'Montserrat-Regular',
},
tradeinnerSecond:{
  fontSize:15,
    color:'black',
    fontFamily:'Montserrat-Regular',
  marginBottom:'3%'
},  
fitImage: {
  borderRadius: 20,
},
fitImageWithSize: {
  height: 100,
  width: 30,
},
contentContainer: {
  borderColor: 'white',
  flexDirection:'row',
  justifyContent: 'flex-start',
  alignItems: 'flex-start',    
},
    });

    export default withNavigation(AppContainer);