import React, { Component } from 'react';
import { View,Dimensions ,Text,StyleSheet,TouchableOpacity,Image} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { ScrollView } from 'react-native-gesture-handler';
import { withNavigation } from 'react-navigation';
import ViewMoreText from 'react-native-view-more-text';
import {  Content } from "native-base";

var {height, width} = Dimensions.get('window');
 class ProfileAbout extends Component{
    constructor(){
      super();
      this.state={
          layout:{
              height:height,
              width:width,
            },
            baseURL:'http://api.neutralairpartner.com',
            height: '',
            isloading: false
      }
      
    }

  

    componentDidMount(){
        global.height= this.state.height
       
    }

   
    render() {
         if (this.state.isloading) {
        return (
          <OrientationLoadingOverlay
          visible={true}
          color="#303147"
          indicatorSize="large"
          messageFontSize={14}
          style={{
            position:'absolute', left:0, right:0, bottom:0, top:0 }}
          >
        </OrientationLoadingOverlay>
         );
       } else{
        return(
           
            <View
            onLayout={(event)=>{
                var {x,y,width,height}= event.nativeEvent.layout;
                console.log(height,"height1234")
                this.props.getHeight(height)
                // setTimeout(() => {
                //   this.props.getHeight(height)
                // }, 500);
              }} 
                style={{width: this.state.layout.width, backgroundColor:'transparent',alignItems:'flex-start'}}>
             
               <View style={styles.dataView}>
                    <View style={styles.head}>
                        <Text style={styles.headText}>About Company</Text>
                    </View>

                    <View style={styles.body}>
                        <Text style={styles.upperbodyText}>{global.description}</Text>
                 
                    </View>
               </View>    

               <View style={styles.dataView}>
                    <View style={styles.head}>
                        <Text style={styles.headText}>MAWB Information</Text>
                    </View>

                    <View style={styles.body}>
                        <Text style={styles.upperbodyText}>{global.mawbaddress}</Text>
                    </View>
               </View>  

               <View style={styles.dataView}>
                    <View style={styles.head}>
                        <Text style={styles.headText}>Destination Notes</Text>
                    </View>

                    <View style={styles.body}>
                        <Text style={styles.upperbodyText}>{global.destinationnotes}</Text>
                    </View>
               </View>  

               <View style={styles.dataView}>
                    <View style={styles.head}>
                        <Text style={styles.headText}>Bank Details</Text>
                    </View>

                    <View style={styles.body}>
                                    <View style={{flexDirection:'row',height:'auto',width:'auto',marginTop:'4%'}}>
                                      <Text style={styles.bodyTexthead}>Bank Name</Text>
                                      <Text style={styles.bodyText}>{global.bankname.toLowerCase()}</Text>
                                     
                                      </View>

                                      <View style={{flexDirection:'row',height:'auto',width:'auto',marginTop:'4%'}}>
                                      <Text style={styles.bodyTexthead}>Account Name</Text>
                                      <Text style={styles.bodyText}>{global.accountname.toLowerCase()}</Text>
                                    
                                      </View>

                                      <View style={{flexDirection:'row',height:'auto',width:'auto',marginTop:'4%'}}>
                                      <Text style={styles.bodyTexthead}>Account Number</Text>
                                      <Text style={styles.bodyText}>{global.accountnumber}</Text>
                                 
                                      </View>

                                      <View style={{flexDirection:'row',height:'auto',width:'auto',marginTop:'4%'}}>
                                      <Text style={styles.bodyTexthead}>SWIFT</Text>
                                      <Text style={styles.bodyText}>{global.swift}</Text>
                                
                                      </View>

                                      <View style={{flexDirection:'row',height:'auto',width:'auto',marginTop:'4%'}}>
                                      <Text style={styles.bodyTexthead}>Telephone</Text>
                                      <Text style={styles.bodyText}>{global.telephone}</Text>
                                    
                                      </View>

                                      <View style={{flexDirection:'row',height:'auto',width:'auto',marginTop:'4%'}}>
                                      <Text style={styles.bodyTexthead}>FAX</Text>
                                      <Text style={styles.bodyText}> {global.fax}</Text>
                                  
                                      </View>

                                      <View style={{flexDirection:'row',height:'auto',width:'auto',marginTop:'4%'}}>
                                      <Text style={styles.bodyTexthead}>Address</Text>
                                      <Text style={styles.bodyText}>{global.bankaddress}</Text>
                                   
                                      </View>
                    </View>
               </View>               
               
             
             </View>
           
          
          );
      }}
  }
  
  
  var styles = StyleSheet.create({
    container: {
      flex: 1,
      height:hp('38%'),
      width:wp('100%'),
      justifyContent: 'flex-start',
      alignItems:'center',
      backgroundColor:'#303147'
    },
  
    dataView:{
        height:'auto',
        width:wp('85%'),
        marginLeft:wp('3%'),
        backgroundColor:'transparent',
         marginTop:hp('5%')
    },
  
 
    head:{
       width:'100%',
       height:'auto',
       backgroundColor:'transparent',
       alignItems:'center',
       flexDirection:'row'
    },

    headText:{
        fontSize:18,
        paddingLeft:'1%',
        color:'#332863',
        
        fontFamily:'Montserrat-Regular',
    },
    body:{
        width:'100%',
        height:'auto',
        backgroundColor:'transparent',
        marginTop:7,
        alignItems:'flex-start',
        //justifyContent:'flex-start'
     },
     bodyTexthead:{
        fontSize:14,
        paddingLeft:'1%',
        color:'grey',
       width:wp('30%'),
        fontFamily:'Montserrat-Regular',
    },

     bodyText:{
        fontSize:14,
        color:'grey',
        width: wp('55%'),
        //textAlignVertical: "center",
        fontFamily:'Montserrat-Light',
    },
    upperbodyText:{
        fontSize:14,
        color:'grey',
       
        //textAlignVertical: "center",
        fontFamily:'Montserrat-Light',
    }
     
  
    });

    export default withNavigation(ProfileAbout)