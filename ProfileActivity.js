import React, { Component } from 'react';
import { View,Dimensions ,Text,StyleSheet,TouchableOpacity,ImageBackground,Image,FlatList} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { ScrollView } from 'react-native-gesture-handler';
import ElevatedView from 'react-native-elevated-view'
import { withNavigation } from 'react-navigation';
import LinearGradient from 'react-native-linear-gradient';
import { ActivityIndicator } from 'react-native-paper';
import {  Content } from "native-base";



var {height, width} = Dimensions.get('window');
 class ProfileActivity extends Component{
    constructor(){
      super();
      this.state={
          layout:{
              height:height,
              width:width,
            },
            baseURL:'http://api.neutralairpartner.com',
            flightApi:'', scroll:false, origin:'',destination:'', flight:'',
            height:'auto'
      } 
    }
   

    scrollControl=()=>{
      this.setState({scroll: false})
    }


 componentDidMount(){
     global.height= this.state.height
 }

 componentWillUnmount(){
    this.setState({isloading:true})
}

    render() {
        if (this.state.isloading) {
            return (
              <ActivityIndicator 
                  animating={true} 
                  color={'#303147'} 
                  size={'small'}
                  style={{ position:'absolute', left:5, right:5, bottom:5, top:5 }}/>
             );
           } else if(global.promo.length>0){
               
        return(
           
            <View
            onLayout={(event)=>{
                var {x,y,width,height}= event.nativeEvent.layout;
                this.props.getHeight(height)
              }}  
                style={{ width: this.state.layout.width, backgroundColor:'transparent',marginTop:'3%'}}>
           
            {/* <FlatList
            data={global.promo}
            scrollEnabled={false}
            renderItem={({ item: data }) => {
            
              return (   
                <ElevatedView style={{height:hp('95%'), width: wp('100%'), backgroundColor:'transparent',marginTop:'4%'}}>  
                  <View style={styles.promohead}>
                    <View style={{width:'15%',height:'70%',backgroundColor:'transparent'}}>
                    <ImageBackground source={require('./Nap_Ios/destination.png')} style={{height:'88%',width:'97%',alignSelf:'flex-end',marginTop:'2%'}}></ImageBackground>
                    </View>

                    <View style={{width:'85%',height:'100%',backgroundColor:'transparent',}}>
                        <Text style={styles.promoheadtext}>{global.companyName}</Text>
                        <Text style={styles.promosecondarytext}>{global.country} </Text>
                        <Text style={styles.promodatetext}>Posted On: {data.createDate.slice(0,data.createDate.indexOf('T'))}</Text>
                    </View>
                  </View>
                    <View style={styles.startToend}>
                            <Image source={require('./Nap_Ios/pin2.png')} style={{height:'55%',width:'4%',marginTop:'3%'}}></Image>
                            <Image source={require('./Nap_Ios/plane2.png')} style={{height:'55%',width:'8%',marginTop:'2.5%'}}></Image>
                            <Image source={require('./Nap_Ios/pin2.png')} style={{height:'55%',width:'4%',marginTop:'3%'}}></Image>
                    </View>

                    <View style={styles.startToend2}>
                          <Text style={styles.startToendtext}>BRU</Text>
                          <Text style={styles.startToendtext}>MNG</Text>
                          <Text style={styles.startToendtext}>IST</Text>
                    </View>

                    <View style={styles.image}></View>

                    <View style={styles.info}>
                            <Image source={require('./Nap_Ios/download.png')} style={{height:'75%',width:'6%'}}></Image>
                            <Text style={{fontFamily:'Montserrat-Regular',fontSize:11,color:'black'}}>Airport pdf flat rate document.pdf</Text>
                            <TouchableOpacity>
                            <Text style={{fontFamily:'Montserrat-Regular',fontSize:12,color:'green'}}>Download</Text>
                            </TouchableOpacity>
                    </View>
 
                    <View style={styles.infotext}>
                        <View style={{width:'85%',height:'auto',backgroundColor:'transparent',marginLeft:'2.5%'}}>
                        <Text style={styles.promoAditionalNotes}>{data.additionalNotes}</Text>
                        </View>
                    </View>
                    
                    <View style={styles.infotextfooter}>
                            <Text style={{fontFamily:'Montserrat-light',fontSize:12,color:'grey',marginTop:'10%'}}>Ends On:{data.endDate.slice(0,data.endDate.indexOf('T'))}</Text>
                            <TouchableOpacity style={{width:'35%',height:'55%',alignItems:'flex-start',backgroundColor:'transparent',marginTop:'5%'}}>
                                <LinearGradient colors={['#00cc66','#00ff99']} style={{width:'100%',height:'80%',borderRadius:45,alignItems:'center'}}>
                                    <Text style={{ fontFamily:'Montserrat-Regular',fontSize:14,color:'white',paddingTop:'11%'}}>Send Message</Text>
                                </LinearGradient>
                            </TouchableOpacity>
                        </View>
                </ElevatedView>
               
               )
                    }}
                        keyExtractor={(item, index) => index.toString()} 
                        style={{ height:'auto'}}/>  */}
                        <Text>sdsdsdsdsd</Text>
                        <Text>sdsdsdsdsd</Text>
                        <Text>sdsdsdsdsd</Text>
                        <Text>sdsdsdsdsd</Text>
                        <Text>sdsdsdsdsd</Text>
                        <Text>sdsdsdsdsd</Text>
                        <Text>sdsdsdsdsd</Text>
                        <Text>sdsdsdsdsd</Text>
                        
             </View>
                 
             
          );
      }else{
          return(
              <View>
              <Text style={styles.null}>No Data Found</Text>
              </View>
          )

          
      }
    }
  }
  
  
  var styles = StyleSheet.create({
   
    promohead:{
        height:'16%',
        width: '100%',
        backgroundColor:'white',
        flexDirection:'row',
        justifyContent:'flex-start'
    } ,
    promoheadtext:{
        fontFamily:'Montserrat-Light',
        fontSize:15,
        color:'black',paddingLeft:'2%'
    },
    promosecondarytext:{
        fontFamily:'Montserrat-Light',
        fontSize:12,
        color:'grey',paddingLeft:'2%'
    },
    promodatetext:{
        fontFamily:'Montserrat-Light',
        fontSize:13,
        color:'grey',
        marginTop:'4%',paddingLeft:'2%'
    }, 
    startToend:{
        height:'6%',
        width:'80%',
        backgroundColor:'transparent',
        flexDirection:'row',
        marginLeft:'5%',
        marginTop:'5%',
        alignSelf:'flex-start',
        justifyContent:'space-around',
       },
    startToendtext:{
        fontFamily:'Montserrat-Light',
        fontSize:12,
        color:'grey'  
    },
    startToend2:{
        height:'5%',
        width:'80%',
        backgroundColor:'transparent',
        flexDirection:'row',
        marginLeft:'5%',
        alignSelf:'flex-start',
        justifyContent:'space-around',
    },
    image:{
        height:'27%',
        width:'100%',
        backgroundColor:'black'
    },
    info:{
       marginTop:'1%',
        height:'5%',
        marginLeft:'1%',
        width:'94%',
        backgroundColor:'transparent',
        flexDirection:'row',
        justifyContent:'space-between'
    },
    infotext:{
        marginTop:'5%',
        height:'15%',
        width:'100%',
        backgroundColor:'transparent',
        alignSelf:'flex-start',
        
        },

        infotextfooter:{
            height:'14%',
            width:'90%',
            backgroundColor:'transparent',
            flexDirection:'row',
            marginLeft:'2.5%',
            justifyContent:'space-between'
            },

            promoAditionalNotes:{
                fontFamily:'Montserrat-Light',
                fontSize:13,
                color:'grey'  
            },
            null:{
                color:'#D3D3D3',
                fontFamily:'Montserrat-Regular',
                alignSelf:'center',
                fontSize:16,
                marginTop:hp('10%')
            }
  
    });

    export default withNavigation(ProfileActivity);