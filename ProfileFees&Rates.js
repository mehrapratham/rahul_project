import React, { Component } from 'react';
import { View,Dimensions ,Text,StyleSheet,FlatList,} from 'react-native';
import { ActivityIndicator } from 'react-native-paper';
import { withNavigation } from 'react-navigation';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { ScrollView } from 'react-native-gesture-handler';
import {  Content } from "native-base";



var {height, width} = Dimensions.get('window');
 class ProfileFees extends Component{
    constructor(){
      super();
      this.state={
          layout:{
              height:height,
              width:width,
            },
           
            rates:[],
            height:'auto',
            baseURL:'http://api.neutralairpartner.com',
            importhandover:'', fob:'', dap:'',
            fee:'', price:'',webservice:0, isloading:true,
            handovername:'',handoverfee:'',fobname:'',fobfee:'',dapname:'',dapfee:'',
            HandoverData:[], FobData:[], DapData:[], isloading:true
      }
    }
    componentWillMount(){
      
         
          // console.log(JSON.stringify(global.importhandover))
          // console.log(JSON.stringify( global.fob))
          // console.log(JSON.stringify(global.dap))
          global.height= this.state.height
          this.setState({isloading:false})
    }

   


    render() {
      if (this.state.isloading) {
        return (
          <ActivityIndicator 
              animating={true} 
              color={'#303147'} 
              size={'small'}
              style={{ position:'absolute', left:5, right:5, bottom:5, top:5 }}/>
         );
       } else{
        return(
         
            <View 
            onLayout={(event)=>{
              var {x,y,width,height}= event.nativeEvent.layout;
              this.props.getHeight(height)
            }} 
              style={{ width:this.state.layout.width, backgroundColor:'transparent'}}>
             
                    {/* ImportHandoverData */}
                        <View style={styles.dataView}>
                            <View style={styles.head}>
                                <Text style={styles.headText}>Import Handover Fees</Text>
                            </View>
                            <View style={styles.key}>
                                <Text style={styles.bodyTextRight}>Name</Text>
                                <Text style={styles.bodyTextLeft}>Fee</Text>
                            </View>

                            <FlatList
                               data={global.importhandover}
                               scrollEnabled={false}
                              renderItem={({ item: data }) => {
                                
                                if(data.importhandoverFee!=null && data.importhandoverPrice!=null){
                                return(
                                  <View style={styles.body}>
                                    <View style={{width:'50%',height:'100%',backgroundColor:'transparent'}}>
                                      <Text style={styles.detailLeft}>{data.importhandoverFee}</Text>
                                    </View>
                                    <View style={{width:'30%',height:'100%',backgroundColor:'transparent',alignItems:'center'}}>
                                      <Text style={styles.detailRight}>{data.importhandoverPrice}</Text>
                                    </View>
                                  </View>
                                )}else if(data.importhandoverFee==null && data.importhandoverPrice!=null){
                                  return(
                                    <View style={styles.body}>
                                      <View style={{width:'50%',height:'100%',backgroundColor:'transparent'}}>
                                        <Text style={styles.detailLeft}>N/A</Text>
                                      </View>
                                      <View style={{width:'30%',height:'100%',backgroundColor:'transparent',alignItems:'center'}}>
                                        <Text style={styles.detailRight}>{data.importhandoverPrice}</Text>
                                      </View>
                                    </View>
                                  )
                                }else if(data.importhandoverFee!=null && data.importhandoverPrice==null){
                                  return(
                                    <View style={styles.body}>
                                      <View style={{width:'50%',height:'100%',backgroundColor:'transparent'}}>
                                        <Text style={styles.detailLeft}>{data.importhandoverFee}</Text>
                                      </View>
                                      <View style={{width:'30%',height:'100%',backgroundColor:'transparent',alignItems:'center'}}>
                                        <Text style={styles.detailRight}>N/A</Text>
                                      </View>
                                    </View>
                                  )
                                }else{
                                  return(
                                    <View style={styles.body}>
                                      <View style={{width:'50%',height:'100%',backgroundColor:'transparent'}}>
                                        <Text style={styles.detailLeft}>N/A</Text>
                                      </View>
                                      <View style={{width:'30%',height:'100%',backgroundColor:'transparent',alignItems:'center'}}>
                                        <Text style={styles.detailRight}>N/A</Text>
                                      </View>
                                    </View>
                                  )
                                }
                              }}
                            keyExtractor={(item, index) => index.toString()} style={{ width:'100%', height: '100%'}}/> 
                        </View>  
                    {/* ImportHandoverData */}

                    
                    {/* FOBData */}
                        <View style={styles.dataView}>
                            <View style={styles.head}>
                                <Text style={styles.headText}>FOB/EXW Charges</Text>
                            </View>
                            <View style={styles.key}>
                                <Text style={styles.bodyTextRight}>Name</Text>
                                <Text style={styles.bodyTextLeft}>Fee</Text>
                            </View>
                          
                            <FlatList
                               data={global.fob}
                               scrollEnabled={false}
                              renderItem={({ item: data }) => {
                              
                                if(data.fobFee!=null && data.fobPrice!=null){
                                  return(
                                    <View style={styles.body}>
                                      <View style={{width:'50%',height:'100%',backgroundColor:'transparent'}}>
                                        <Text style={styles.detailLeft}>{data.fobFee}</Text>
                                      </View>
                                      <View style={{width:'40%',height:'100%',backgroundColor:'transparent',alignItems:'center'}}>
                                        <Text style={styles.detailRight}>{data.fobPrice}</Text>
                                      </View>
                                    </View>
                                  )}else if(data.fobFee==null && data.fobPrice!=null){
                                    return(
                                      <View style={styles.body}>
                                        <View style={{width:'50%',height:'100%',backgroundColor:'transparent'}}>
                                          <Text style={styles.detailLeft}>N/A</Text>
                                        </View>
                                        <View style={{width:'30%',height:'100%',backgroundColor:'transparent',alignItems:'center'}}>
                                          <Text style={styles.detailRight}>{data.fobPrice}</Text>
                                        </View>
                                      </View>
                                    )
                                  }else if(data.fobFee!=null && data.fobPrice==null){
                                    return(
                                      <View style={styles.body}>
                                        <View style={{width:'50%',height:'100%',backgroundColor:'transparent'}}>
                                          <Text style={styles.detailLeft}>{data.fobFee}</Text>
                                        </View>
                                        <View style={{width:'30%',height:'100%',backgroundColor:'transparent',alignItems:'center'}}>
                                          <Text style={styles.detailRight}>N/A</Text>
                                        </View>
                                      </View>
                                    )
                                  }else{
                                    return(
                                      <View style={styles.body}>
                                        <View style={{width:'50%',height:'100%',backgroundColor:'transparent'}}>
                                          <Text style={styles.detailLeft}>N/A</Text>
                                        </View>
                                        <View style={{width:'30%',height:'100%',backgroundColor:'transparent',alignItems:'center'}}>
                                          <Text style={styles.detailRight}>N/A</Text>
                                        </View>
                                      </View>
                                    )
                                  }
                                }}
                            keyExtractor={(item, index) => index.toString()} style={{ width:'100%', height: '100%'}}/> 
                          </View>  
                     {/* FOBData */}


                     {/* DAPData */}
                        <View style={styles.dataView}>
                            <View style={styles.head}>
                                <Text style={styles.headText}>DAP/DDP Charges</Text>
                            </View> 
                            <View style={styles.key}>
                                <Text style={styles.bodyTextRight}>Name</Text>
                                <Text style={styles.bodyTextLeft}>Fee</Text>
                            </View>
                            
                            <FlatList 
                               data={global.dap}
                               scrollEnabled={false}
                              renderItem={({ item: data }) => {
                      
                                if(data.dapFee!=null && data.dapPrice!=null){
                                  return(
                                    <View style={styles.body}>
                                      <View style={{width:'50%',height:'100%',backgroundColor:'transparent'}}>
                                        <Text style={styles.detailLeft}>{data.dapFee}</Text>
                                      </View>
                                      <View style={{width:'30%',height:'100%',backgroundColor:'transparent',alignItems:'center'}}>
                                        <Text style={styles.detailRight}>{data.dapPrice}</Text>
                                      </View>
                                    </View>
                                  )}else if(data.dapFee==null && data.dapPrice!=null){
                                    return(
                                      <View style={styles.body}>
                                        <View style={{width:'50%',height:'100%',backgroundColor:'transparent'}}>
                                          <Text style={styles.detailLeft}>N/A</Text>
                                        </View>
                                        <View style={{width:'30%',height:'100%',backgroundColor:'transparent',alignItems:'center'}}>
                                          <Text style={styles.detailRight}>{data.dapPrice}</Text>
                                        </View>
                                      </View>
                                    )
                                  }else if(data.dapFee!=null && data.dapPrice==null){
                                    return(
                                      <View style={styles.body}>
                                        <View style={{width:'50%',height:'100%',backgroundColor:'transparent'}}>
                                          <Text style={styles.detailLeft}>{data.dapFee}</Text>
                                        </View>
                                        <View style={{width:'30%',height:'100%',backgroundColor:'transparent',alignItems:'center'}}>
                                          <Text style={styles.detailRight}>N/A</Text>
                                        </View>
                                      </View>
                                    )
                                  }else{
                                    return(
                                      <View style={styles.body}>
                                        <View style={{width:'50%',height:'100%',backgroundColor:'transparent'}}>
                                          <Text style={styles.detailLeft}>N/A</Text>
                                        </View>
                                        <View style={{width:'30%',height:'100%',backgroundColor:'transparent',alignItems:'center'}}>
                                          <Text style={styles.detailRight}>N/A</Text>
                                        </View>
                                      </View>
                                    )
                                  }
                                }}
                            keyExtractor={(item, index) => index.toString()} style={{ width:'100%', height: '100%'}}/> 
                           
                        </View>  
                      {/* DAPData */}


                         {/* DAPData */}
                        <View style={styles.dataView}>
                            <View style={styles.head}>
                                <Text style={styles.headText}>Rates</Text>
                            </View> 
                            <View style={styles.key}>
                                <Text style={styles.bodyTextRight}>Name</Text>
                                <Text style={styles.bodyTextLeft}>Fee</Text>
                            </View>
                            
                            <FlatList 
                               data={global.rates}
                               scrollEnabled={false}
                              renderItem={({ item: data }) => {
                      
                                if(data.ratesFee!=null && data.ratesPrice!=null){
                                  return(
                                    <View style={styles.body}>
                                      <View style={{width:'50%',height:'100%',backgroundColor:'transparent'}}>
                                        <Text style={styles.detailLeft}>{data.ratesFee}</Text>
                                      </View>
                                      <View style={{width:'30%',height:'100%',backgroundColor:'transparent',alignItems:'center'}}>
                                        <Text style={styles.detailRight}>{data.ratesPrice}</Text>
                                      </View>
                                    </View>
                                  )}else if(data.ratesFee==null && data.ratesPrice!=null){
                                    return(
                                      <View style={styles.body}>
                                        <View style={{width:'50%',height:'100%',backgroundColor:'transparent'}}>
                                          <Text style={styles.detailLeft}>N/A</Text>
                                        </View>
                                        <View style={{width:'30%',height:'100%',backgroundColor:'transparent',alignItems:'center'}}>
                                          <Text style={styles.detailRight}>{data.ratesPrice}</Text>
                                        </View>
                                      </View>
                                    )
                                  }else if(data.ratesFee!=null && data.ratesPrice==null){
                                    return(
                                      <View style={styles.body}>
                                        <View style={{width:'50%',height:'100%',backgroundColor:'transparent'}}>
                                          <Text style={styles.detailLeft}>{data.ratesFee}</Text>
                                        </View>
                                        <View style={{width:'30%',height:'100%',backgroundColor:'transparent',alignItems:'center'}}>
                                          <Text style={styles.detailRight}>N/A</Text>
                                        </View>
                                      </View>
                                    )
                                  }else{
                                    return(
                                      <View style={styles.body}>
                                        <View style={{width:'50%',height:'100%',backgroundColor:'transparent'}}>
                                          <Text style={styles.detailLeft}>N/A</Text>
                                        </View>
                                        <View style={{width:'30%',height:'100%',backgroundColor:'transparent',alignItems:'center'}}>
                                          <Text style={styles.detailRight}>N/A</Text>
                                        </View>
                                      </View>
                                    )
                                  }
                                }}
                            keyExtractor={(item, index) => index.toString()} style={{ width:'100%', height: '100%'}}/> 
                           
                        </View>  
                      {/* DAPData */}
            
           </View>

          
          );}}
      }
  
  
  
  var styles = StyleSheet.create({
  
    
      dataView:{
          height:hp('40%'),
          width:'100%',
         backgroundColor:'transparent',
         marginTop:'3%'
           
      },
    
   
      head:{
         width:'100%',
         height:'auto',
         backgroundColor:'transparent',
         alignItems:'center',
         flexDirection:'row'
      },
  
      headText:{
          fontSize:15,
          paddingLeft:'3%',
          color:'#332863',
        
          fontFamily:'Montserrat-Regular',
      },
     
  
       bodyTextRight:{
          fontSize:16,
          color:'grey',
          fontFamily:'Montserrat-Regular',
          marginTop:12,
          marginLeft:'2%'
      },
      bodyTextLeft:{
        fontSize:16,
        color:'grey',
        fontFamily:'Montserrat-Regular',
        marginTop:12,
        marginLeft:'60%'
    },

      key:{
            height:hp('7%'),
            width:'100%',
            backgroundColor:'#D3D3D3',
            flexDirection:'row',
            marginTop:'3%'
      },
      body:{
        height:'auto',
        width:'90%',
        backgroundColor:'transparent',
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between',
        marginTop:'2%'
      },
      detailLeft:{
        fontSize:14,
        color:'black',
        fontFamily:'Montserrat-Light',
        marginTop:5,
        alignItems:'center',
         marginLeft:'6%'
    },
    detailRight:{
        fontSize:14,
        color:'black',
        fontFamily:'Montserrat-Light',
        marginTop:5,
        alignItems:'center'
        // marginLeft:'40%'
    },
  
    });

    export default withNavigation(ProfileFees)

                            