import React, { Component } from 'react';
import { View,Dimensions ,Text,StyleSheet,TouchableOpacity,Image,FlatList} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { ScrollView } from 'react-native-gesture-handler';
import ElevatedView from 'react-native-elevated-view'
import { withNavigation } from 'react-navigation';

import {  Content } from "native-base";

var {height, width} = Dimensions.get('window');
 class ProfilePersonel extends Component{
    constructor(){
      super();
      this.state={
          layout:{
              height:height,
              width:width,
            },
            array:[{name:'rk'},{name:'rk'},{name:'rk'}],
           height: 'auto'

      }
    }

    componentDidMount(){
      global.height= this.state.height
    }

    render() {
        return(
          
            <View
            onLayout={(event)=>{
              var {x,y,width,height}= event.nativeEvent.layout;
              this.props.getHeight(height)
            }}  
              style={{ width: '100%', backgroundColor:'transparent',alignItems:'center'}}>
                <FlatList
                  style={{ width:'100%',height:'auto'}}
                  data={this.state.array}
                  scrollEnabled={false}
                  renderItem={({ item: data }) => {
                      return(
                        <ElevatedView 
                        elevation={4}
                        style={styles.personal}>

                                <View style={styles.ImageView}></View>
                                <View style={styles.detailView}>
                                  <View style={styles.name}>
                                    <Text style={styles.nameText}>Brandon Smith</Text>
                                    <Text style={styles.designation}>Operations manager</Text>
                                  </View>
                                  <View style={styles.contactDetails}>
                                      <View style={{flexDirection:'row',height:'auto',width:'auto',}}>
                                        <Text style={styles.bodyTexthead}>Phone: </Text>
                                        <Text style={styles.bodyText}>+852 13402142550</Text>
                                      </View>
                                      <View style={{flexDirection:'row',height:'auto',width:'auto',}}>
                                        <Text style={styles.bodyTexthead}>Cellphone: </Text>
                                        <Text style={styles.bodyText}>+852 1340214255</Text>
                                      </View>
                                      <View style={{flexDirection:'row',height:'auto',width:'auto',}}>
                                        <Text style={styles.bodyTexthead}>Fax: </Text>
                                        <Text style={styles.bodyText}>+852 1340214255</Text>
                                      </View>
                                      <View style={{flexDirection:'row',height:'auto',width:'auto',}}>
                                        <Text style={styles.bodyTexthead}>Email: </Text>
                                        <Text style={styles.bodyText}>tony@airconsol.com</Text>
                                      </View>
                                      <View style={{flexDirection:'row',height:'auto',width:'auto',}}>
                                        <Text style={styles.bodyTexthead}>Skype: </Text>
                                        <Text style={styles.bodyText}>tony_airconsol</Text>
                                      </View>
                                  </View>
                                </View>

                        </ElevatedView>
                      );
                }}
                keyExtractor={(item, index) => index.toString()}/>
            
            </View>
              
          
          );
      }
  }
  
  
  var styles = StyleSheet.create({
    personal: {
      
      height:hp('28%'),
      width:wp('90%'),
      justifyContent: 'flex-start',
      alignSelf:'center',
      borderRadius:3,
      backgroundColor:'white',
      marginTop:hp('4%'),
      flexDirection:'row'
    },
  
    ImageView:{
        height:'45%',
        width:'24%',
        borderRadius:50,
        backgroundColor:'#303147',
        marginTop:'10%',
        marginLeft:'2%'
    },
    detailView:{
      height:'95%',
      width:'65%',
      backgroundColor:'transparent',
      borderRadius:3,
      
      
    },
    name:{
      height:'30%',
      width:'90%',
      alignSelf:'center',
      backgroundColor:'transparent',
       marginTop:'4%'
    },
    nameText:{
      fontSize:18,
      color:'black',
      fontFamily:'Montserrat-Bold'
    },
    designation:{
      fontSize:13,
      color:'black',
      fontFamily:'Montserrat-Light'
    },
    contactDetails:{
      height:'58%',
      width:'100%',
      backgroundColor:'transparent',
      marginTop:'6%',
      marginLeft:0
    },
    bodyTexthead:{
      fontSize:13,
      paddingLeft:'5%',
      color:'black',
      fontFamily:'Montserrat-Light',
  },
  bodyText:{
   fontSize:13,
   color:'black',
  
   fontFamily:'Montserrat-Light',
},
     
  
    });

    export default withNavigation(ProfilePersonel)