import React, { Component } from 'react';
import { View,Dimensions ,Text,StyleSheet,TouchableOpacity,ImageBackground,Image,FlatList} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { ScrollView } from 'react-native-gesture-handler';
import ElevatedView from 'react-native-elevated-view'
import { withNavigation } from 'react-navigation';
import Header from './Header';
import LinearGradient from 'react-native-linear-gradient';
import { Form, Item, Picker } from 'native-base';
import { ActivityIndicator } from 'react-native-paper';





var {height, width} = Dimensions.get('window');
 class Promo extends Component{
    constructor(){
      super();
      this.state={
          layout:{
              height:height,
              width:width,
            },
            isloading:true,
            baseURL:'http://api.neutralairpartner.com',
            promoApi:'/v1/my_promos', 
            selected2: 'key0',      
            promos:[],
            

      } 
    }

    componentDidMount(){
      
             this.onFetchPromoInfo(global.token)
       
    }

      //for promo info
      async onFetchPromoInfo(token){
        try {
          let response = await fetch(
            'http://api.neutralairpartner.com/v1/my_promos?page=1',
           {
             'method': 'GET',
             headers: { 
               'Accept': 'application/json',
              'Content-Type': 'application/json',
              'Authorization':'Bearer'+' '+token
              },
          }
         );
          if (response.status >= 200 && response.status < 300) {
            response.json().then(data => {
              data.map((item,key)=>{
                   this.state.promos.push(item) 
              
            })

            this.setState({isloading:false})
          
                
              }); 
              
           } else{ 
             alert('JWT token not found')
           }
        } catch (errors) {
           alert(errors);
          } 
        }

      
      

    onValueChange2(value) {
        this.setState({
          selected2: value
        });
      }
  
    render() {
      if (this.state.isloading) {
        return (
          <ActivityIndicator 
              animating={true} 
              color={'#303147'} 
              size={'large'}
              style={{ position:'absolute', left:0, right:0, bottom:0, top:0 }}/>
         );
       } else{
        return(
          
            <View style={{height:this.state.layout.height, width: this.state.layout.width, backgroundColor:'#f9f9f9',alignItems:"center"}}>
               <Header/>
              
                <Form style={{marginTop:hp('2%')}}>
                <Item picker rounded style={{backgroundColor:'#0099cc'}}>
                <Picker 
                mode="dropdown"
                // iosIcon={}
                style={{ width: '100%' }}
                textStyle={{color:'white',fontFamily:'Montserrat-Bold', fontSize:13}}
                selectedValue={this.state.selected2}
                onValueChange={this.onValueChange2.bind(this)}>
                <Picker.Item label="Show All Promos" value="key0" />
                <Picker.Item label="Show Only My Promos" value="key1" />
                
                </Picker>
                 </Item>
                </Form>

                <FlatList
                data={this.state.promos}
                scrollEnabled={true}
                showsVerticalScrollIndicator={false}
                renderItem={({ item: data, index }) => {
                  console.log(JSON.stringify(data))
              return ( 
                <ElevatedView
                    elevation={4} 
                    style={{ width:wp('90%'),backgroundColor:'white', height:this.state.layout.height/1.1, alignSelf:'flex-start',  borderBottomLeftRadius:4, borderBottomLeftRadius:4,backgroundColor:'white',alignSelf:'center',marginTop:'5%'}}>
                    <View style={styles.promohead}>
                        <View style={{height:'100%',width:'18%',backgroundColor:'transparent',alignItems:'center'}}>
                        <ImageBackground source={require('./Nap_Ios/destination.png')} style={{height:'80%',width:'97%',marginTop:'2%'}}></ImageBackground>
                        </View>
                        <View style={{width:'82%',height:'100%',alignItems:'flex-start',backgroundColor:'transparent',}}>
                                <Text style={styles.promoheadtext}>hh</Text>
                                <Text style={styles.promosecondarytext}> yy</Text>
                                <Text style={styles.promodatetext}>Posted On: </Text>
                        </View>
                    </View>

                    <View style={styles.startToend}>
                            <Image source={require('./Nap_Ios/pin2.png')} style={{height:'55%',width:'3.5%'}}></Image>
                            <Image source={require('./Nap_Ios/plane2.png')} style={{height:'55%',width:'8%'}}></Image>
                            <Image source={require('./Nap_Ios/pin2.png')} style={{height:'55%',width:'3.5%'}}></Image>
                    </View>

                    <View style={styles.startToend2}>
                          <Text style={styles.startToendtext}></Text>
                          <Text style={styles.startToendtext}></Text>
                          <Text style={styles.startToendtext}></Text>
                    </View>
                    
                    <View style={styles.image}></View>

                    <View style={styles.info}>
                            <Image source={require('./Nap_Ios/download.png')} style={{height:'50%',width:'5%'}}></Image>
                            <Text style={{fontFamily:'Montserrat-Regular',fontSize:11,color:'black'}}>Airport pdf flat rate document.pdf</Text>
                            <TouchableOpacity>
                            <Text style={{fontFamily:'Montserrat-Regular',fontSize:12,color:'green'}}>Download</Text>
                            </TouchableOpacity>
                    </View>
 
                    <View style={styles.infotext}>
                            <Text style={styles.promoAditionalNotes}></Text>
                    </View>

                    <View style={styles.infotextfooter}>
                            <Text style={{fontFamily:'Montserrat-light',fontSize:12,color:'grey',marginTop:'2%'}}>Ends On: </Text>
                            <TouchableOpacity style={{width:'35%',height:'75%',alignItems:'flex-start',backgroundColor:'transparent',}}>
                                <LinearGradient colors={['#00cc66','#00ff99']} style={{width:'100%',height:'80%',borderRadius:45,alignItems:'center'}}>
                                    <Text style={{ fontFamily:'Montserrat-Regular',fontSize:13,color:'white',paddingTop:'11%'}}>Send Message</Text>
                                </LinearGradient>
                            </TouchableOpacity>
                        </View>
                   
                </ElevatedView>
 
                )
                }}
                    keyExtractor={(item, index) => index.toString()} /> 
               
             </View>

            
          );
      }}
  }
  
  
  var styles = StyleSheet.create({
   
    promohead:{
        height:'15%',
        width: '100%',
        backgroundColor:'transparent',
        flexDirection:'row',
        justifyContent:'flex-start'
    } ,
    promoheadtext:{
        fontFamily:'Montserrat-Light',
        fontSize:15,
        color:'black',paddingLeft:'2%',
        marginTop:'1%'
    },
    promosecondarytext:{
        fontFamily:'Montserrat-Light',
        fontSize:14,
        color:'grey',paddingLeft:'0.3%'
    },
    promodatetext:{
        fontFamily:'Montserrat-Light',
        fontSize:13,
        color:'grey',
        marginTop:'4%',paddingLeft:'1.7%'
    }, 
    startToend:{
        height:'8%',
        width:'90%',
       marginTop:"3%",
        backgroundColor:'transparent',
        flexDirection:'row',
        justifyContent:'space-around',
        alignSelf:'center',alignItems:'flex-end'
    },
    startToend2:{
        height:'6%',
        width:'92%',
        marginTop:'1%',
        backgroundColor:'transparent',
        flexDirection:'row',
        justifyContent:'space-around',
        alignSelf:'center',alignItems:'flex-start'
    },
    startToendtext:{
        fontFamily:'Montserrat-Light',
        fontSize:12,
        color:'grey'  
    },
    image:{
        height:'32%',
        width:'100%',
        backgroundColor:'#2B303A'
    },
    info:{
        marginTop:'5%',
        height:'8%',
        width:'95%',
        backgroundColor:'transparent',
        flexDirection:'row',
        alignSelf:'center',
        justifyContent:'space-between'
    },
    infotext:{
        height:'auto',
        width:'95%',
        backgroundColor:'transparent',
        alignSelf:'center',
        },

        infotextfooter:{
            height:'15%',
            width:'95%',
            backgroundColor:'transparent',
            flexDirection:'row',
            justifyContent:'space-between',
            alignSelf:'center',marginTop:'6%'
            },

            promoAditionalNotes:{
                fontFamily:'Montserrat-Regular',
                fontSize:13,
                color:'grey'  
            }
  
    });

    export default withNavigation(Promo);





    //{data.createDate.slice(0,data.createDate.indexOf('T'))}
    //{data.originAirport.iata}
    //{data.promosAirline.iata}
    //{data.destinationAirport.iata}
    //{data.additionalNotes}
    //{data.endDate.slice(0,data.endDate.indexOf('T'))}