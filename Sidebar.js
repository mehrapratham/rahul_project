import React, { Component } from 'react';
import { View,Dimensions ,Text,StyleSheet,TouchableWithoutFeedback,Image,ImageBackground} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { withNavigation, SafeAreaView} from 'react-navigation';
import { TouchableOpacity } from 'react-native-gesture-handler';


const {width}= Dimensions.get('screen')

 class Sidebar extends Component{
    

        render() {
           return(
       
            <View style={{flex:1,width:'100%',backgroundColor:'#303147',alignItems:'center'}}>
                
                 <View style={{flex:2, width:'100%', backgroundColor:'transparent',alignItems:'center', marginTop:30}}>
                    <Text style={{fontSize:12, fontFamily:'Montserrat-Light',color:'white'}}>MENU</Text>
                </View>
                <View style={{flex:95, width:'80%', backgroundColor:'transparent',justifyContent:'space-around',alignItems:'center',marginTop:'40%'}}>
               
               

                <TouchableOpacity onPress={()=>this.props.navigation.navigate('Home')}>
                <View style={{height:'25%', width:'100%', backgroundColor:'transparent',alignItems:'center'}}>
                    <Image source={require('./Nap_Ios/puzzle.png')} style={{width: 20, height: 20}}></Image>
                </View>
                </TouchableOpacity>

                <TouchableOpacity onPress={()=>this.props.navigation.navigate('Promo')}>
                <View style={{height:'25%', width:'100%', backgroundColor:'transparent', alignItems:'center'}}>
                    <Image source={require('./Nap_Ios/fire.png')} style={{width: 20, height: 20}}></Image>
                </View>
                </TouchableOpacity>

                <TouchableOpacity onPress={()=>this.props.navigation.navigate('Geolocator')}>
                <View style={{height:'25%', width:'100%', backgroundColor:'transparent', alignItems:'center'}}>
                    <Image source={require('./Nap_Ios/map.png')} style={{width: 20, height: 20}}></Image>
                </View>
                </TouchableOpacity>

                <TouchableOpacity onPress={()=>this.props.navigation.navigate('Alert')}>
                <View style={{height:'25%', width:'100%', backgroundColor:'transparent', alignItems:'center'}}>
                    <Image source={require('./Nap_Ios/alert.png')} style={{width: 20, height: 20}}></Image>
                </View>
                </TouchableOpacity>
                
                <TouchableOpacity onPress={()=>this.props.navigation.navigate('ChatScreen')}>
                <View style={{height:'25%', width:'100%', backgroundColor:'transparent', alignItems:'center'}}>
                    <Image source={require('./Nap_Ios/message.png')} style={{width: 20, height: 20}}></Image>
                </View>
                </TouchableOpacity>

                <TouchableOpacity onPress={()=>this.props.navigation.navigate('ChatScreen')}>
                <View style={{height:'25%', width:'100%', backgroundColor:'transparent', alignItems:'center'}}>
                    <Image source={require('./Nap_Ios/more.png')} style={{width: 20, height: 20}}></Image>
                </View>
                </TouchableOpacity>
                </View>
              
                      
             </View>
             
          );
      }
  }



  export default withNavigation(Sidebar)