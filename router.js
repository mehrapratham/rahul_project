import {createMaterialTopTabNavigator,createAppContainer} from 'react-navigation'
import { View,Dimensions ,Text,StyleSheet} from 'react-native';
import ProfileAbout from './ProfileAbout'
import ProfileActivity from './ProfileActivity'
import ProfilePersonel from './ProfilePersonel'
import ProfileFees from './ProfileFees&Rates'

var {height, width} = Dimensions.get('window');
const AppNav= createMaterialTopTabNavigator({
    About: {screen: ProfileAbout},
    Personel:{screen: ProfilePersonel},
    Rates:{screen: ProfileFees},
    Activity:{screen: ProfileActivity},
  },{
    tabBarOptions:{
        
      scrollEnabled: true,
      labelStyle: {
        fontSize: 11,
        fontWeight:'600'
      },
      tabStyle: {
        alignSelf:'center',
        width: Dimensions.get('window').width / 4,
      },
      style: {
        backgroundColor: 'white',
      },
      indicatorStyle: {
        backgroundColor: '#303147'
      },
      activeTintColor:'#303147',
      inactiveTintColor:'black',
      inactiveBackgroundColor:'black'
    },
    tabBarPosition:'top',
  
    
  }
  
  )
  const AppContain= createAppContainer(AppNav)
  export default AppContain
 
  